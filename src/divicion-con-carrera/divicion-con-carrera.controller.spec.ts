import { Test, TestingModule } from '@nestjs/testing';
import { DivicionConCarreraController } from './divicion-con-carrera.controller';

describe('DivicionConCarrera Controller', () => {
  let controller: DivicionConCarreraController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DivicionConCarreraController],
    }).compile();

    controller = module.get<DivicionConCarreraController>(DivicionConCarreraController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
