import { Test, TestingModule } from '@nestjs/testing';
import { AspirantseController } from './aspirantse.controller';

describe('Aspirantse Controller', () => {
  let controller: AspirantseController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AspirantseController],
    }).compile();

    controller = module.get<AspirantseController>(AspirantseController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
