import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne } from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { UserEntity } from "../../user/user.entity";
import { UsuarioEntity } from "../../usuario/UsuarioEntity/usuarioBean.entity";
import { DivicionAcademicaEntity } from "../../divicion-academica/DivicionAcademicaEntity/divicionAcademicaBean.entity";
import { TiempoEntity } from "../../tiempo/tiempoEntity/tiempoBean.entity";
import { ImparteMateriasEntity } from "../../imparte-materias/imparteMateriasEntity/imparteMateriasBean.entity";
import { EvaluacionEntity } from "../../evaluacion/EvaluacioinEntity/evaluacionBean.entity";
import { CalificacionAspiranteEntity } from "../../calificacion-aspirante/CalificacionAspiranteEntity/calificacionAspiranteBean.entity";


@Entity('trabajador')
export class TrabajadorEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombre: string

  @Column()
  primerApellido: string

  @Column()
  segundoApellido: string

  @Column()
  correoInstitucional: string

  @Column()
  correoPersonal: string

  @Column()
  cedula: string

  @Column()
  nivelMaximo: string

  @Column()
  titulo: string

  @Column()
  telefonoCasa: string

  @Column()
  telefonoMovil: string

  @Column()
  curp: string

  @Column()
  estado: string

  @Column()
  semblanza: string

  @Column()
  sexo: string

  @ManyToOne(type => TiempoEntity, tiempoTrabajador => tiempoTrabajador.trabajadorTiempo)
  tiempoTrabajador: TiempoEntity

  @ManyToOne(type => DivicionAcademicaEntity, divicionTrabajador => divicionTrabajador.trabajadorDivicion)
  divicionTrabajador: DivicionAcademicaEntity

  @OneToMany(type => ImparteMateriasEntity, imparteTrabajador => imparteTrabajador.trabajadorImparte)
  imparteTrabajador: ImparteMateriasEntity

  @OneToMany(type => EvaluacionEntity, evaluacionTrabajador => evaluacionTrabajador.trabajadorEvaluacion)
  evaluacionTrabajador: EvaluacionEntity
  
  @OneToMany(type=>CalificacionAspiranteEntity, calificacionId=>calificacionId.id_trabajador)
  calificacionId
}
