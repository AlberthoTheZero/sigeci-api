import { Test, TestingModule } from '@nestjs/testing';
import { GrupoProfesorController } from './grupo-profesor.controller';

describe('GrupoProfesor Controller', () => {
  let controller: GrupoProfesorController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GrupoProfesorController],
    }).compile();

    controller = module.get<GrupoProfesorController>(GrupoProfesorController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
