import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne, Double, OneToOne, JoinColumn} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { type } from "os";
import { DivicionAcademicaEntity } from "../../divicion-academica/DivicionAcademicaEntity/divicionAcademicaBean.entity";
import { MateriaCarreraEntity } from "../../materia-carrera/MateriaCarreraEntity/materiaCarreraBean.entity";
import { DivicionConCarreraEntity } from "../../divicion-con-carrera/DivicionConCarreraEntity/divicionConCarreraBean.entity";
import { GrupoCareraEntity } from "../../grupo-carrera/GrupoCarreraEntity/grupoCarreraBean.entity";
import { AspiranteEntity } from "../../aspirantse/AspiranteEntity/aspiranteBean.entity";


@Entity('carrera')
export class CarreraEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombreCarrera:string

  @Column()
  acronimoCarrera:string

  @Column()
  status:boolean

  @OneToMany(type=>DivicionConCarreraEntity,asignaCarrera=>asignaCarrera.carreraConDivicion)
  asignaCarrera:DivicionConCarreraEntity

  @OneToMany(type=>GrupoCareraEntity,carreraConGrupo=>carreraConGrupo.asignaGrupoCarrera)
  carreraConGrupo:GrupoCareraEntity

  @OneToMany(type=>AspiranteEntity,aspiranteCarrera=>aspiranteCarrera.carreraAspirante)
  aspiranteCarrera:AspiranteEntity


}
