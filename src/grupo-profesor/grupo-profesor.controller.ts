import { Controller, Post, Body, Get } from '@nestjs/common';
import { GrupoProfesorService } from './grupo-profesor.service';

@Controller('grupo-profesor')
export class GrupoProfesorController {

    constructor(private grupoProfesorService: GrupoProfesorService) { }

    @Post('listasParaRegistro')
    async listasParaRegistro(@Body('carrera') carrera: any): Promise<any> {
        if (!carrera) {
            return { respose: { error: true, message: "JSON vacio" } }
        }

        return await this.grupoProfesorService.listasParaRegistro(carrera)
    }

    @Get('lista')
    async lista(): Promise<any> {
      
        return await this.grupoProfesorService.listaPorCarrea()
    }

    @Post('registro')
    async registro(@Body('registro') registro: any): Promise<any> {
        if (!registro) {
            return { response: { error: true, message: "JSON vacio" } }
        }
        return await this.grupoProfesorService.registro(registro)
    }


    @Post('modificar')
    async modificar(@Body('modificar') modificar: any): Promise<any> {
        if (!modificar) {
            return { response: { error: true, message: "JSON vacio" } }
        }
        return await this.grupoProfesorService.modificar(modificar)
    }

    @Post('eliminar')
    async eliminarGrupoProfesor(@Body('grupo_profesor') grupo_profesor: any): Promise<any> {
        return await this.grupoProfesorService.eliminarGrupoProfesor(grupo_profesor)
    }

}
