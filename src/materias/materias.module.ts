import { Module } from '@nestjs/common';
import { MateriasController } from './materias.controller';
import { MateriasService } from './materias.service';
import { MateriasEntity } from './MateriasEntity/materiaBean.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([MateriasEntity])],
  controllers: [MateriasController],
  providers: [MateriasService]
})
export class MateriasModule {}
