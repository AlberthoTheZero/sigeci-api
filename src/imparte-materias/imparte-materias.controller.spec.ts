import { Test, TestingModule } from '@nestjs/testing';
import { ImparteMateriasController } from './imparte-materias.controller';

describe('ImparteMaterias Controller', () => {
  let controller: ImparteMateriasController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ImparteMateriasController],
    }).compile();

    controller = module.get<ImparteMateriasController>(ImparteMateriasController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
