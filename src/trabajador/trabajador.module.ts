import { Module } from '@nestjs/common';
import { TrabajadorController } from './trabajador.controller';
import { TrabajadorService } from './trabajador.service';
import { TrabajadorEntity } from './trabajadorEntity/trabajadorBean.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([TrabajadorEntity])],
  controllers: [TrabajadorController],
  providers: [TrabajadorService]
})
export class TrabajadorModule {}
