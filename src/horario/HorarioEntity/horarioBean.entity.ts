import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { type } from "os";
import { GrupoEntity } from "../../grupo/GrupoEntity/grupoBean.entity";
import { GrupoHorarioEntity } from "../../grupo-horario/GrupoHorarioEntity/grupoHorarioBean.entity";
import { GrupoProfesorEntity } from "../../grupo-profesor/GrupoProfesorEntity/grupoProfesorBean.entity";

@Entity('horario')
export class HorarioEntity {

  @PrimaryGeneratedColumn()
  id: number;
  
  @Column()
  horaInicio:string

  @Column()
  horaFin:string

  @OneToMany(type=>GrupoHorarioEntity,grupoHorariohorario=>grupoHorariohorario.horarioGrupoHorario)
  grupoHorariohorario:GrupoHorarioEntity

  @OneToMany(type=>GrupoProfesorEntity,grupoProfesorHorario=>grupoProfesorHorario.horarioGrupoPofesor)
  grupoProfesorHorario:GrupoProfesorEntity


  
 
}
