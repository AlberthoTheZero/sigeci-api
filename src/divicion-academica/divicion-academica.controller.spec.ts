import { Test, TestingModule } from '@nestjs/testing';
import { DivicionAcademicaController } from './divicion-academica.controller';

describe('DivicionAcademica Controller', () => {
  let controller: DivicionAcademicaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DivicionAcademicaController],
    }).compile();

    controller = module.get<DivicionAcademicaController>(DivicionAcademicaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
