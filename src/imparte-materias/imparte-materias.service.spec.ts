import { Test, TestingModule } from '@nestjs/testing';
import { ImparteMateriasService } from './imparte-materias.service';

describe('ImparteMateriasService', () => {
  let service: ImparteMateriasService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ImparteMateriasService],
    }).compile();

    service = module.get<ImparteMateriasService>(ImparteMateriasService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
