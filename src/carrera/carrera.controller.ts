import { Controller, Get, Post, Body } from '@nestjs/common';
import { CarreraService } from './carrera.service';

@Controller('carrera')
export class CarreraController {

    constructor(private carreraService: CarreraService) { }


    @Get('lista')
    async lista(): Promise<any> {
        return this.carreraService.lista();
    }

    @Post('registro')
    async registrarCarrera(@Body('carrera') carrera: any): Promise<any> {
        if (!carrera) {
            return { error: true, message: "Object esta vacio" }
        }
        return await this.carreraService.registroCarrera(carrera);
    }

    @Post('modificacion')
    async modificarCarrera(@Body('carrera') carrera: any): Promise<any> {
        if (!carrera) {
            return { error: true, message: "Object vacío" }
        }
        return await this.carreraService.modificarCarrera(carrera);
    }

    @Post('eliminacion')
    async eliminarCarrera(@Body('carrera') carrera: any): Promise<any>{
        if (!carrera){
            return { error: true, message: "Objeto vacío"}
        }
        return await this.carreraService.eliminarCarrera(carrera);
    }




}
