import { Controller, Get, Post, Body } from '@nestjs/common';
import { ImparteMateriasService } from './imparte-materias.service';

@Controller('imparte-materias')
export class ImparteMateriasController {

    constructor(private imparteMateriasService:ImparteMateriasService){
    }



    @Post('listasParaRegistro')
    async listasParaRegistrar(@Body('carrera') carrera:any):Promise<any>{
        if (!carrera){
            return {response:{error:true,message:"JSON vacio"}}
        }
        return await this.imparteMateriasService.listasParaRegistrar(carrera);
    }

    @Post('listaMaterias')
    async listaMaterias(@Body('carrera') carrera:any):Promise<any>{
        return await this.imparteMateriasService.listaMaterias(carrera);
    }

    
    @Get('lista')
    async lista():Promise<any>{
        return await this.imparteMateriasService.lista()
    }

    @Post('registro')
    async registro(@Body('registro') registro:any):Promise<any>{
    if(!registro){
        return {reponse:{error:true,message:"JSON vacio"}}
    }else{
        return await this.imparteMateriasService.regstro(registro)
    }
    }
    
    @Post('modificar')
    async modificar(@Body('modificar') modificar:any):Promise<any>{
    if(!modificar){
        return {reponse:{error:true,message:"JSON vacio"}}
    }else{
        return await this.imparteMateriasService.modificar(modificar)
    }
    }

    @Post('carreras')
    async carreras(@Body('division') division:any):Promise<any>{
        return await this.imparteMateriasService.listaCarreras(division);
    }

    @Post('eliminar')
    async eliminar(@Body('eliminar') eliminar:any):Promise<any>{
        if(!eliminar){
            return{ response:{error:true,message:"JSON vacio"}}
        }
        return await this.imparteMateriasService.eliminar(eliminar);
    }
    

}
