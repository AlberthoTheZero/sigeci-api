import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { type } from "os";
import { GrupoEntity } from "../../grupo/GrupoEntity/grupoBean.entity";
import { TrabajadorEntity } from "../../trabajador/trabajadorEntity/trabajadorBean.entity";
import { MateriasEntity } from "../../materias/MateriasEntity/materiaBean.entity";
import { GrupoProfesorEntity } from "../../grupo-profesor/GrupoProfesorEntity/grupoProfesorBean.entity";

@Entity('imparteMaterias')
export class ImparteMateriasEntity {

  @PrimaryGeneratedColumn()
  id: number;
  
  @ManyToOne(type=>TrabajadorEntity,trabajadorImparte=>trabajadorImparte.imparteTrabajador)
  trabajadorImparte:TrabajadorEntity

  @ManyToOne(type=>MateriasEntity,materiasImparte=>materiasImparte.imparteMaterias)
  materiasImparte:MateriasEntity

  @OneToMany(type=>GrupoProfesorEntity,grupoProfesorImparteMaterias=>grupoProfesorImparteMaterias.imparteMateriasGrupoProfesor)
  grupoProfesorImparteMaterias:GrupoProfesorEntity
 
}
