import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne, Double, OneToOne, JoinColumn} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { TrabajadorEntity } from "../../trabajador/trabajadorEntity/trabajadorBean.entity";
import { type } from "os";
import { AspiranteEntity } from "../../aspirantse/AspiranteEntity/aspiranteBean.entity";
import { CarreraEntity } from "../../carrera/CarreraEntity/carreraBean.entity";
import { GrupoEntity } from "../../grupo/GrupoEntity/grupoBean.entity";
import { DivicionConCarreraEntity } from "../../divicion-con-carrera/DivicionConCarreraEntity/divicionConCarreraBean.entity";


@Entity('divicionAcademica')
export class DivicionAcademicaEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column() 
  nombreDivicion:string

  @OneToMany(type=>TrabajadorEntity,trabajadorDivicion=>trabajadorDivicion.divicionTrabajador)
  trabajadorDivicion:TrabajadorEntity

  @OneToMany(type=>DivicionAcademicaEntity,asignaDivicion=>asignaDivicion.asignaDivicion)
  asignaDivicion:DivicionConCarreraEntity

  @OneToMany(type=>CarreraEntity,carreraConDivicion=>carreraConDivicion.asignaCarrera)
  carreraConDivicion:CarreraEntity


}
