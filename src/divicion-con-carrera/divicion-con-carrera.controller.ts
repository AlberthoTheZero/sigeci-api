import { Controller, Get, Body, Post } from '@nestjs/common';
import { DivicionConCarreraService } from './divicion-con-carrera.service';

@Controller('divicion-con-carrera')
export class DivicionConCarreraController {
constructor(private divicionCarreraService:DivicionConCarreraService){}



@Get('listaParaRegistro')
async listas():Promise<any>{
    return await this.divicionCarreraService.listasParaRegistro();
}

@Post('registrar')
async registro(@Body('informacion') informacion):Promise<any>{
    console.log(JSON.stringify(informacion))
    if(!informacion){
     return {response:{error:true,message:"JSON vacio"}}
    }
    return await this.divicionCarreraService.registro(informacion);
}

@Get('lista')
async lista():Promise<any>{
    return await this.divicionCarreraService.lista();
}

@Post('modificar')
async modificar(@Body('modificar') modificar:any):Promise<any>{
if(!modificar){
    return {response:{error:true,message:"JSON vacio"}}
}
return await this.divicionCarreraService.modificar(modificar);
}

@Post('eliminar')
async eliminar(@Body('eliminar')  eliminar:any):Promise<any>{
    if(!eliminar){
        return {response:{error:true,message:"JSON vacio"}}
    }
    return await this.divicionCarreraService.eliminar(eliminar)
}




}
