import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GrupoProfesorEntity } from './GrupoProfesorEntity/grupoProfesorBean.entity';
import { Repository } from 'typeorm';
import { async } from 'rxjs/internal/scheduler/async';

@Injectable()
export class GrupoProfesorService {
    constructor(
        @InjectRepository(GrupoProfesorEntity)
        private readonly grupoProfesorRepository: Repository<GrupoProfesorEntity>
      ) { }


      async listasParaRegistro(carrera:any):Promise<any>{
          let listaDocentes = await this.grupoProfesorRepository.query("select im.id,nombre,primerApellido,segundoApellido,titulo,nombreMateria from impartematerias as im join trabajador as t on t.id=im.trabajadorImparteId join materias as m on im.materiasImparteId=m.id join materiacarrera as mc on mc.materiasAsignaCarreraId=m.id where mc.carreraAsignaMateriasId="+carrera.id+";")
          let listaGrupos= await this.grupoProfesorRepository.query("select gc.id,gradoGrupo,letraGrupo,nombreCarrera,acronimoCarrera from grupo_carrera as gc join grupo as g on g.id=gc.grupoConCarreraId join carrera as c on c.id=gc.asignaGrupoCarreraId where c.id="+carrera.id+";")
          let horarios= await this.grupoProfesorRepository.query("select * from horario;")  
          return {response:{error:false,listaDocentes:listaDocentes,listaGrupos:listaGrupos,horarios:horarios}} 
        }
    
    async listaPorCarrea(){
        return await this.grupoProfesorRepository.query("select gp.id,nombre,primerApellido,segundoApellido,g.gradoGrupo,g.letraGrupo,c.nombreCarrera,h.horaInicio,h.horaFin,im.id as idDocente,h.id as idHorario,gc.id as idGrupo,c.id as idCarrera from grupo_profesor as gp join impartematerias as im on im.id=gp.imparteMateriasGrupoProfesorId join trabajador as t on t.id=im.trabajadorImparteId join materias as m on m.id=im.materiasImparteId join grupo_carrera as gc on gc.id=gp.grupoCarreraGrupoProfesorId join carrera as c on c.id=gc.asignaGrupoCarreraId join grupo as g on g.id=gc.grupoConCarreraId join horario as h on h.id=gp.horarioGrupoPofesorId;")
    }
     async registro(registro:any){
        let horariosOcupados :any[]
        try{
            
             horariosOcupados=await this.grupoProfesorRepository.query("select t.id,nombre,primerApellido,segundoApellido,horaInicio,horaFin from trabajador as t left join impartematerias as im on im.trabajadorImparteId=t.id left join grupo_profesor as gp on gp.imparteMateriasGrupoProfesorId=im.id left join horario as h on h.id=gp.horarioGrupoPofesorId where t.id="+registro.idDocente+" and h.id="+registro.idHorario+";")
            
            
            if(horariosOcupados.length>0){
                        return {response:{error:true,messge:"Horario registrado",horariosOcupados:horariosOcupados}}
                }else{
                    await this.grupoProfesorRepository.query("insert into grupo_profesor values(null,"+registro.idGrupo+","+registro.idDocente+","+registro.idHorario+");")
                return {response:{error:false,messge:"El registro se ha realizado",}}
                }
         }catch(err){
            return {response:{error:false,messge:"Ocurrio un error interno",errorMessage:err}}
         }
     }

     
    async modificar(modificar:any){
        try{
            let horariosOcupados:any[]=await this.grupoProfesorRepository.query("select t.id,nombre,primerApellido,segundoApellido,horaInicio,horaFin from trabajador as t left join impartematerias as im on im.trabajadorImparteId=t.id left join grupo_profesor as gp on gp.imparteMateriasGrupoProfesorId=im.id left join horario as h on h.id=gp.horarioGrupoPofesorId where t.id="+modificar.idDocente+" and h.id="+modificar.idHorario+";")
               if(horariosOcupados.length>0){
                       return {response:{error:true,messge:"Horario registrado",horariosOcupados:horariosOcupados}}
               }else{
                   await this.grupoProfesorRepository.query("update grupo_profesor set grupoCarreraGrupoProfesorId="+modificar.idGrupo+",imparteMateriasGrupoProfesorId="+modificar.idDocente+",horarioGrupoPofesorId="+modificar.idHorario+" where id="+modificar.id+";")
               return {response:{error:false,message:"La modificacion se ha realizado",}}
               }
        }catch(err){
           return {response:{error:false,message:"Ocurrio un error interno",errorMessage:err}}
        }
    }

    async eliminarGrupoProfesor(grupo_profesor:any):Promise<any>{
        try {
            let sqlEliminarGrupoProfesor = "delete from grupo_profesor where id = '"+grupo_profesor.id+"';"
            await this.grupoProfesorRepository.query(sqlEliminarGrupoProfesor)
            return {response:{error:false, message:"Grupo-profesor eliminado correctamente"}}
        } catch (err) {
            return {response:{error:true, message:"Error al eliminar grupo-profesor", errorMessage:err}}
        }
    }

}
