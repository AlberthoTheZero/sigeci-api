import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';


@Entity('pregunta')
export class PreguntaEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  pregunta:string

  @Column()
  estatus:number

 
}
