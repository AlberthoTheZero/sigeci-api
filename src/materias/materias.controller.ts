import { Controller, Get, Post, Body } from '@nestjs/common';
import { MateriasService } from './materias.service';

@Controller('materias')
export class MateriasController {

    constructor(private materiasService: MateriasService) { }


    @Get('lista')
    async lista() {
        return await this.materiasService.lista();
    }

    @Post('registro')
    async registro(@Body('registro') registro: any): Promise<any> {
        if (!registro) {
            return { response: { error: true, message: "JSON vacio" } }
        }
        return await this.materiasService.registro(registro);
    }

    @Post("modificar")
    async modificar(@Body('modificar') modificar: any) {
        if (!modificar) {
            return { respose: { error: true, message: "JSON vacio" } }
        }
        return await this.materiasService.modificar(modificar)
    }


    @Post("eliminar")
    async eliminar(@Body('eliminar') eliminar: any) {
        if (!eliminar) {
            return { respose: { error: true, message: "JSON vacio" } }
        }
        return await this.materiasService.eliminar(eliminar)
    }

}
