import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { type } from "os";
import { GrupoHorarioEntity } from "../../grupo-horario/GrupoHorarioEntity/grupoHorarioBean.entity";

@Entity('grupo_carrera_aspirante')
export class GrupoCarreraAspiranteEntity {

  @PrimaryGeneratedColumn()
  id: number;
  
  @Column()
  grupoCarreraID:number

  @Column()
  aspiranteId:number
 
}
