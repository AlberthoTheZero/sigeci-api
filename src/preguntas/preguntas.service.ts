import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PreguntaEntity } from './PreguntasEntity/preguntasBean.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PreguntasService {


    constructor(
        @InjectRepository(PreguntaEntity)
        private readonly preguntasRepository: Repository<PreguntaEntity>
    ) { }


    async vistaAspirante(): Promise<any> {
        return await this.preguntasRepository.query("select * from pregunta where estatus=1; ")
    }

    async vistaServiciosEscoles() {
        return await this.preguntasRepository.query("select * from pregunta;")
    }

    async registro(registro: any): Promise<any> {
        try {
            await this.preguntasRepository.query("insert into pregunta values(null,'" + registro.pregunta + "',0);")
            return { response: { error: false, message: "El registro se realizo" } }
        } catch (err) {
            return { response: { error: true, message: "Ocurrio un error interno", errorMessage: err } }
        }
    }

    async modificar(modificar: any): Promise<any> {
        try {
            await this.preguntasRepository.query("update pregunta set pregunta='" + modificar.pregunta + "' where id=" + modificar.id + ";")
            return { response: { error: false, message: "La modificacion se realizo" } }
        } catch (err) {
            return { response: { error: true, message: "Ocurrio un error interno", errorMessage: err } }
        }
    }

    async activar(activar: any): Promise<any> {
        try {
            let estado=""
            if (activar.opcion == 1) {
                await this.preguntasRepository.query("update pregunta set estatus=1 where id!=3;")
                estado="ACTIVADO"
            } else {
                await this.preguntasRepository.query("update pregunta set estatus=0 where id!=3;")
                estado="DESACTIVADO"
            }
            return {response:{error:false,message:"El estado cambio a "+ estado}}
        } catch (err) {
            return { response: { error: true, message: "Ocurrio un error interno", errorMessage: err } }
        }
    }

    async eliminar(eliminar:any):Promise<any>{
        try{
          await this.preguntasRepository.query("update pregunta set estatus=3 where id="+eliminar.id+";")
          return {response:{error:false,message:"Se ha eliminado el registro"}}
        }catch(err){
            return { response: { error: true, message: "Ocurrio un error interno", errorMessage: err } }
        
        }
    }
    
    async recuperar(recuperar:any):Promise<any>{
        try{
          await this.preguntasRepository.query("update pregunta set estatus=1 where id="+recuperar.id+";")
          return {response:{error:false,message:"Se activo la pregunta"}}
        }catch(err){
            return { response: { error: true, message: "Ocurrio un error interno", errorMessage: err } }
        }
    }


}
