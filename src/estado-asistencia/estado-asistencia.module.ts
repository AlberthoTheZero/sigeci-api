import { Module } from '@nestjs/common';
import { EstadoAsistenciaController } from './estado-asistencia.controller';
import { EstadoAsistenciaService } from './estado-asistencia.service';

@Module({
  controllers: [EstadoAsistenciaController],
  providers: [EstadoAsistenciaService]
})
export class EstadoAsistenciaModule {}
