import { Test, TestingModule } from '@nestjs/testing';
import { GrupoHorarioService } from './grupo-horario.service';

describe('GrupoHorarioService', () => {
  let service: GrupoHorarioService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GrupoHorarioService],
    }).compile();

    service = module.get<GrupoHorarioService>(GrupoHorarioService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
