import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne, Double, OneToOne, JoinColumn} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { type } from "os";
import { DivicionAcademicaEntity } from "../../divicion-academica/DivicionAcademicaEntity/divicionAcademicaBean.entity";
import { GrupoCareraEntity } from "../../grupo-carrera/GrupoCarreraEntity/grupoCarreraBean.entity";

@Entity('grupo')
export class GrupoEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  gradoGrupo:number

  @Column()
  letraGrupo:string

  @Column()
  status:boolean

  @OneToMany(type=>GrupoCareraEntity,asignaGrupo=>asignaGrupo.grupoConCarrera)
  asignaGrupo:GrupoCareraEntity

}
