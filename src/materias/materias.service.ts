import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MateriasEntity } from './MateriasEntity/materiaBean.entity';
import { Repository } from 'typeorm';
import { async } from 'rxjs/internal/scheduler/async';

@Injectable()
export class MateriasService {
    constructor(
        @InjectRepository(MateriasEntity)
        private readonly materiasRepository: Repository<MateriasEntity>
    ) { }


    async lista(): Promise<any> {
        return await this.materiasRepository.query("select * from materias;")
    }

    async registro(registro: any): Promise<any> {
        try {
            await this.materiasRepository.query("insert into materias values(null,'" + registro.nombreMateria + "');")
            return { response: { error: false, message: "Se registro correctamente" } }
        } catch (err) {
            return { response: { error: true, message: "Error interno en el sistema", errorMessage: err } }
        }

    }

    async modificar(modificar: any): Promise<any> {
        try {
            let siExiste: any[] = await this.materiasRepository.query("select * from materias where id=" + modificar.id + ";")
            if (siExiste.length <= 0) {
                return { response: { error: true, message: "El registro no existe" } }
            } else {
                await this.materiasRepository.query("update materias set nombreMateria='" + modificar.nombreMateria + "' where id=" + modificar.id + ";")
                return { response: { error: true, message: "El registro se modifico" } }
            }

        } catch (err) {
            return { response: { error: true, message: "Error interno en el sistema", errorMessage: err } }
        }
    }

    async eliminar(eliminar: any) {
        try {
            let siExiste: any[] = await this.materiasRepository.query("select * from materias where id=" + eliminar.id + ";")
            if (siExiste.length <= 0) {
                return { response: { error: true, message: "El registro no existe" } }
            } else {
                await this.materiasRepository.query("delete from materias where id=" + eliminar.id + ";")
                return { response: { error: true, message: "El registro se eliminó" } }
            }

        } catch (err) {
            return { response: { error: true, message: "No se puede eliminar, contiene relación con un registro", errorMessage: err } }
        }
    }




}
