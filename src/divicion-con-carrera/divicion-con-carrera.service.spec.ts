import { Test, TestingModule } from '@nestjs/testing';
import { DivicionConCarreraService } from './divicion-con-carrera.service';

describe('DivicionConCarreraService', () => {
  let service: DivicionConCarreraService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DivicionConCarreraService],
    }).compile();

    service = module.get<DivicionConCarreraService>(DivicionConCarreraService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
