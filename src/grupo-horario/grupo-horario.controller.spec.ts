import { Test, TestingModule } from '@nestjs/testing';
import { GrupoHorarioController } from './grupo-horario.controller';

describe('GrupoHorario Controller', () => {
  let controller: GrupoHorarioController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GrupoHorarioController],
    }).compile();

    controller = module.get<GrupoHorarioController>(GrupoHorarioController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
