import { Test, TestingModule } from '@nestjs/testing';
import { GrupoCarreraController } from './grupo-carrera.controller';

describe('GrupoCarrera Controller', () => {
  let controller: GrupoCarreraController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GrupoCarreraController],
    }).compile();

    controller = module.get<GrupoCarreraController>(GrupoCarreraController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
