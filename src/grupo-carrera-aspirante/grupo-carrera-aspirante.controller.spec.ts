import { Test, TestingModule } from '@nestjs/testing';
import { GrupoCarreraAspiranteController } from './grupo-carrera-aspirante.controller';

describe('GrupoCarreraAspirante Controller', () => {
  let controller: GrupoCarreraAspiranteController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GrupoCarreraAspiranteController],
    }).compile();

    controller = module.get<GrupoCarreraAspiranteController>(GrupoCarreraAspiranteController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
