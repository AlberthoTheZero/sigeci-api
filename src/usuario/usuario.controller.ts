import { Controller, Post, Body, Get } from '@nestjs/common';
import { UsuarioEntity } from './UsuarioEntity/usuarioBean.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UsuarioService } from './usuario.service';

@Controller('usuario')
export class UsuarioController {
    constructor(private usuarioService: UsuarioService) { }


    @Post('login')
    async login(@Body('usuario') usuario: any): Promise<any> {
        return await this.usuarioService.login(usuario);
    }

    @Post('modificarClave')
    async modificarClave(@Body('usuario') usuario: any): Promise<any> {
        return await this.usuarioService.modificarClave(usuario)
    }


   

}
