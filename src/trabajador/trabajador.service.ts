import { Injectable } from '@nestjs/common';
import { TrabajadorEntity } from './trabajadorEntity/trabajadorBean.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class TrabajadorService {

  constructor(
    @InjectRepository(TrabajadorEntity)
    private readonly trabajadorRepository: Repository<TrabajadorEntity>
  ) { }


  async registroTrabajador(trabajador: any): Promise<any> {
    let trabajadorInfo

    try {
      let existTrabajador = await this.trabajadorRepository.query("select * from usuario where usuarioNombre='" + trabajador.curp + "' and estatus=1;")

      if (existTrabajador[0].id > 0) {
        return { response: { error: true, message: "Trabajador ya existe" } }
      }
    } catch (err) {
      console.log("Error->" + err)
    }

    try {
      trabajadorInfo = await this.trabajadorRepository.query("insert into trabajador values(" +
        "null," +
        "'" + trabajador.nombre + "'," +
        "'" + trabajador.primerApellido + "'," +
        "'" + trabajador.segundoApellido + "'," +
        "'" + trabajador.correoInstitucional + "'," +
        "'" + trabajador.correoPersonal + "'," +
        "'" + trabajador.cedula + "'," +
        "'" + trabajador.nivelMaximo + "'," +
        "'" + trabajador.titulo + "'," +
        "'" + trabajador.telefonoCasa + "'," +
        "'" + trabajador.telefonoMovil + "'," +
        "'" + trabajador.curp + "'," +
        "'activo'," +
        "'" + trabajador.semblanza + "'," +
        "'" + trabajador.sexo + "'," +
        trabajador.tiempoId + "," +
        trabajador.divicionId +
        ");")
    } catch (err) {
      return { response: { error: true, errorMessage: err, message: "Error to insert Trabajador in TABLE trabajador=> JSON =>" + JSON.stringify(trabajador) } }
    }
    let clave = JSON.stringify(trabajador.curp)
    let SQLInsertarUsuarioTrabajador = "insert into usuario values(" +
      " null," +
      "'" + trabajador.curp + "'," +
      "'" + clave.substring(clave.length - 4, clave.length - 1) + "',1," +
      trabajador.rol + ");";

    try {
      await this.trabajadorRepository.query(SQLInsertarUsuarioTrabajador);
    } catch (err) {
      return { response: { error: true, errorMessage: err, message: "Error to insert TRABJADOR in TABLE USUARIO=> JSON =>" + JSON.stringify(trabajador) } }
    }
    return { response: { error: false, message: "Trabajador Registrado correctamente" } }


  }


  async listasRegistro(): Promise<any> {
    try {

      let listaTiempo = await this.trabajadorRepository.query("select * from tiempo;")
      let listaDivicion = await this.trabajadorRepository.query("select * from divicionacademica;")
      let listaRol = await this.trabajadorRepository.query("select * from rol where nombreRol!='ASPIRANTE';")

      return { response: { error: false, listaTiempo: listaTiempo, listaDivicion: listaDivicion, listaRol: listaRol } }
    } catch (err) {
      return { response: { error: true, errorMessage: err } }
    }

  }


  async modificarTrabajador(trabajador: any): Promise<any> {
    try {
      let sqlModificarTrabajador = "UPDATE trabajador SET nombre = '" + trabajador.nombre + "', primerApellido = '" + trabajador.primerApellido + "', segundoApellido = '" + trabajador.segundoApellido + "', correoInstitucional = '" + trabajador.correoInstitucional + "', correoPersonal = '" + trabajador.correoPersonal + "', cedula = '" + trabajador.cedula + "', nivelMaximo = '" + trabajador.nivelMaximo + "', titulo = '" + trabajador.titulo + "', telefonoCasa = '" + trabajador.telefonoCasa + "', telefonoMovil = '" + trabajador.telefonoMovil + "', estado = '" + trabajador.estado + "', semblanza = '" + trabajador.semblanza + "', sexo = '" + trabajador.sexo + "', tiempoTrabajadorId = '" + trabajador.tiempoTrabajadorId + "', divicionTrabajadorId = '" + trabajador.divicionTrabajadorId + "' WHERE id = " + trabajador.id + ";"
      console.log(sqlModificarTrabajador)
      await this.trabajadorRepository.query(sqlModificarTrabajador)
      return { response: { error: false, message: "Trabajador modificado correctamente" } }
    } catch (err) {
      return { response: { error: true, message: "Error al modificar trabajador", errorMessage: err } }
    }
  }
  async modificarTrabajadorDivision(trabajador: any): Promise<any> {
    try {
      let sqlModificarTrabajador = "UPDATE trabajador SET divicionTrabajadorId="+trabajador.idDivision+", nombre = '" + trabajador.nombre + "', primerApellido = '" + trabajador.primerApellido + "', segundoApellido = '" + trabajador.segundoApellido + "', correoInstitucional = '" + trabajador.correoInstitucional + "', correoPersonal = '" + trabajador.correoPersonal + "', cedula = '" + trabajador.cedula + "', nivelMaximo = '" + trabajador.nivelMaximo + "', titulo = '" + trabajador.titulo + "', telefonoCasa = '" + trabajador.telefonoCasa + "', telefonoMovil = '" + trabajador.telefonoMovil + "', estado = '" + trabajador.estado + "', semblanza = '" + trabajador.semblanza + "', sexo = '" + trabajador.sexo + "', tiempoTrabajadorId = '" + trabajador.tiempoTrabajadorId + "' WHERE id = " + trabajador.id + ";"
      console.log(sqlModificarTrabajador)
      await this.trabajadorRepository.query(sqlModificarTrabajador)
      return { response: { error: false, message: "Trabajador modificado correctamente" } }
    } catch (err) {
      return { response: { error: true, message: "Error al modificar trabajador", errorMessage: err } }
    }
  }

  async listaDocentes(divicion: any): Promise<any> {
    try {
      let sqlConsultaDocentes = "select u.id, u.usuarioNombre, u.estatus,t.nombre,t.primerApellido,t.segundoApellido,t.correoInstitucional,t.correoPersonal,t.cedula,t.nivelMaximo,t.titulo,t.telefonoCasa,t.telefonoMovil,t.curp,t.estado,t.semblanza,t.sexo, r.nombreRol, t.tiempoTrabajadorId, t.divicionTrabajadorId from usuario as u join trabajador as t join rol as r on u.rolUsuarioId=r.id and u.usuarioNombre=t.curp where rolUsuarioId =3 and divicionTrabajadorId= '" + divicion.id + "';"
      return await this.trabajadorRepository.query(sqlConsultaDocentes)
    } catch (err) {
      return { response: { error: true, errorMessage: err } }
    }
  }

  async listaDirectores(): Promise<any> {
    try {
      let sqlConsultaDirectores = "select t.id, u.usuarioNombre, u.estatus,t.nombre,t.primerApellido,t.segundoApellido,t.correoInstitucional,t.correoPersonal,t.cedula,t.nivelMaximo,t.titulo,t.telefonoCasa,t.telefonoMovil,t.curp,t.estado,t.semblanza,t.sexo, r.nombreRol, t.tiempoTrabajadorId, t.divicionTrabajadorId from usuario as u join trabajador as t join rol as r on u.rolUsuarioId=r.id and u.usuarioNombre=t.curp where rolUsuarioId =2;"
      return await this.trabajadorRepository.query(sqlConsultaDirectores)
    } catch (err) {
      return { response: { error: true, errorMessage: err } }
    }
  }

  async eliminarTrabajador(eliminar: any): Promise<any> {
    try {
      let sqlEliminarTrabajador = "delete from trabajador where id = '" + eliminar.id + "';"
      await this.trabajadorRepository.query(sqlEliminarTrabajador)
      return { response: { error: false, message: "Eliminación exitosa." } }
    } catch (err) {
      return { response: { error: true, message: "Error al eliminar, relación existente." } }
    }
  }

}
