import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GrupoEntity } from './GrupoEntity/grupoBean.entity';
import { Repository } from 'typeorm';

@Injectable()
export class GrupoService {
    constructor(
        @InjectRepository(GrupoEntity)
        private readonly grupoRepository: Repository<GrupoEntity>
    ) { }

    async lista(): Promise<any> {
        try {
            let listaGrupos= await this.grupoRepository.query("select id, gradoGrupo, letraGrupo, status from grupo where status =1;")
        return {response:{error:false,message:"Consulta exitosa",listaGrupos:listaGrupos}}
        } catch (err) {
            return { response: { error: true, errorMessage: err } }
        }
    }

    async registrarGrupo(grupo: any): Promise<any> {
        try {
            let sqlRegistroGrupo = "INSERT INTO grupo  VALUES (null, '" + grupo.gradoGrupo + "', '" + grupo.letraGrupo + "', '1');"
            await this.grupoRepository.query(sqlRegistroGrupo)
            return { response: { error: false, message: "Registro de grupo exitoso" } }
        } catch (err) {
            return { response: { error: true, message: "Error al insertar grupo", errorMessage: err } }
        }
    }

    async modificarGrupo(grupo: any): Promise<any> {
        try {
            let sqlModificarGrupo = "UPDATE grupo SET gradoGrupo = '" + grupo.gradoGrupo + "', letraGrupo = '" + grupo.letraGrupo + "', status = '" + grupo.status + "' WHERE id = '" + grupo.id + "';"
            await this.grupoRepository.query(sqlModificarGrupo)
            return { response: { error: false, message: "Grupo modificado correctamente" } }
        } catch (err) {
            return { response: { error: true, message: "Error al modificar grupo", errorMessage: err } }
        }
    }

    async eliminarGrupo(grupo: any): Promise<any> {
        try {
            let sqlEliminarGrupo = "DELETE FROM grupo where id = '" + grupo.id + "'"
            await this.grupoRepository.query(sqlEliminarGrupo)
            return { response: { error: false, message: "Grupo eliminado correctamente" } }
        } catch (err) {
            return { response: { error: true, message: "Error al eliminar grupo", errorMessage: err } }
        }
    }

}
