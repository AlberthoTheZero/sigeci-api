import { Module } from '@nestjs/common';
import { ImparteMateriasController } from './imparte-materias.controller';
import { ImparteMateriasService } from './imparte-materias.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImparteMateriasEntity } from './imparteMateriasEntity/imparteMateriasBean.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ImparteMateriasEntity])],
  controllers: [ImparteMateriasController],
  providers: [ImparteMateriasService]
})
export class ImparteMateriasModule {}
