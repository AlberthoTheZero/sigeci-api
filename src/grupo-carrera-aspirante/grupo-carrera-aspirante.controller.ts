import { Controller, Get, Post, Body } from '@nestjs/common';
import { GrupoCarreraAspiranteService } from './grupo-carrera-aspirante.service';

@Controller('grupo-carrera-aspirante')
export class GrupoCarreraAspiranteController {
    constructor(private grupoCarreraAspiranteService: GrupoCarreraAspiranteService) { }

    @Post('listasParaRegistro')
    async listasParaRegistro(@Body('carrera') carrera: any): Promise<any> {
        if(!carrera){
            return { response:{error:true,message:"JSON vacio"}}
        }
        return await this.grupoCarreraAspiranteService.listasParaRegistro(carrera)
    }


    @Post('registro')
    async registroGrupoCarreraAspirante(@Body('registro') registro: any) {
        if (!registro) {
            return { response: { error: true, message: "Objeto vacío" } }
        } else {
            return await this.grupoCarreraAspiranteService.registroGrupoCarreraAspirante(registro)
        }
    }

    @Post('modificar')
    async modificar(@Body('modificar') modificar: any) {
        if (!modificar) {
            return { response: { error: true, message: "Objeto vacío" } }
        } else {
            return await this.grupoCarreraAspiranteService.modificarGrupoCarreraAspirante(modificar)
        }
    }

    @Get('lista')
    async lista():Promise<any>{
        return await this.grupoCarreraAspiranteService.lista();
    }

    @Post('eliminar')
    async eliminar(@Body('grupoCarreraAspirante') grupoCarreraAspirante: any): Promise<any> {
        return await this.grupoCarreraAspiranteService.eliminarGrupoCarreraAspirante(grupoCarreraAspirante)
    }

}
