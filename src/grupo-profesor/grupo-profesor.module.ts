import { Module } from '@nestjs/common';
import { GrupoProfesorController } from './grupo-profesor.controller';
import { GrupoProfesorService } from './grupo-profesor.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GrupoProfesorEntity } from './GrupoProfesorEntity/grupoProfesorBean.entity';

@Module({
  imports: [TypeOrmModule.forFeature([GrupoProfesorEntity])],
  controllers: [GrupoProfesorController],
  providers: [GrupoProfesorService]
})
export class GrupoProfesorModule {


}
