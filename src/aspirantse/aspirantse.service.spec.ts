import { Test, TestingModule } from '@nestjs/testing';
import { AspirantseService } from './aspirantse.service';

describe('AspirantseService', () => {
  let service: AspirantseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AspirantseService],
    }).compile();

    service = module.get<AspirantseService>(AspirantseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
