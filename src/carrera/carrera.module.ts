import { Module } from '@nestjs/common';
import { CarreraController } from './carrera.controller';
import { CarreraService } from './carrera.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CarreraEntity } from './CarreraEntity/carreraBean.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CarreraEntity])],
  controllers: [CarreraController],
  providers: [CarreraService]
})
export class CarreraModule {}
