import { Test, TestingModule } from '@nestjs/testing';
import { MateriaCarreraController } from './materia-carrera.controller';

describe('MateriaCarrera Controller', () => {
  let controller: MateriaCarreraController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MateriaCarreraController],
    }).compile();

    controller = module.get<MateriaCarreraController>(MateriaCarreraController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
