import { Controller, Post, Body, Get } from '@nestjs/common';
import { DivicionAcademicaService } from './divicion-academica.service';

@Controller('divicion-academica')
export class DivicionAcademicaController {
    constructor(private divicionService: DivicionAcademicaService) { }

    @Post('registro')
    async registrarDivicion(@Body('divicion') divicion: any): Promise<any> {
        if (!divicion) {
            return { error: true, message: "Object esta vacio" }
        }
        return await this.divicionService.registroDivicion(divicion);
    }

    @Post('modificacion')
    async modificarDivicion(@Body('divicion') divicion: any): Promise<any> {
        if (!divicion) {
            return { error: true, message: "Object vacío" }
        }
        return await this.divicionService.modificarDivicion(divicion);
    }

    @Get('lista')
    async lista(): Promise<any> {
        return this.divicionService.lista();
    }

    @Post('eliminar')
    async eliminar(@Body('divicion') divicion: any): Promise<any> {
        return await this.divicionService.eliminarDivisionAcademica(divicion)
    }

}
