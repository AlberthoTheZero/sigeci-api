import { Controller, Get, Post, Body } from '@nestjs/common';
import { CalificacionAspiranteService } from './calificacion-aspirante.service';

@Controller('calificacion-aspirante')
export class CalificacionAspiranteController {
    constructor(private calificacionAspiranteService: CalificacionAspiranteService) { }

    @Post('listaCalificacionesProfesor')
    async listaCalificacionesProfesor(@Body('profesor') profesor: any): Promise<any> {
        return await this.calificacionAspiranteService.listaCalificacionesProfesor(profesor)
    }

    @Post('registro')
    async registroCalificacionAspirante(@Body('registro') registro: any): Promise<any> {
        if (!registro) {
            return { response: { error: true, message: "Objeto vacío" } }
        } else {
            return await this.calificacionAspiranteService.registroCalificacionAspirante(registro)
        }
    }

    @Post('modificar')
    async modificarCalificacionAspirante(@Body('modificar') modificar: any): Promise<any> {
        if (!modificar) {
            return { response: { error: true, message: "Objeto vacío." } }
        } else {
            return await this.calificacionAspiranteService.modificarCalificacionAspirante(modificar)
        }
    }

    @Post('eliminar')
    async eliminarCalificacionAspirante(@Body('eliminar') eliminar: any): Promise<any> {
        return await this.calificacionAspiranteService.eliminarCalificacionAspirante(eliminar)
    }

    @Post('registrarCalificaciones')
    async registrarTodasCalificaciones(@Body('calificaciones') calificaciones: any): Promise<any> {
        return await this.calificacionAspiranteService.registrarCalificacionesAspirantes(calificaciones)
    }

    @Post('calificacionesAspirante')
    async calificacionesAspirante(aspirante: any): Promise<any> {
        return await this.calificacionAspiranteService.listaCalificacionesAspirante(aspirante)
    }
}
