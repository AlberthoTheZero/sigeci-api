import { Controller, Post, Body, Get } from '@nestjs/common';
import { AspirantseService } from './aspirantse.service';

@Controller('aspirantse')
export class AspirantseController {

    constructor(private aspiranteService:AspirantseService){}


    @Post('registro')
    async registroAspirante(@Body('aspirante') aspirante:any):Promise<any>{
    console.log("JSON Recibido: \n"+JSON.stringify(aspirante))
    if(!aspirante){
        return {response:{error:true,message:"Te has registrado correctamente"+JSON.stringify(aspirante)}}
    }
     return await this.aspiranteService.registroAspirante(aspirante);
    }


    @Get('lista')
    async listaApirante():Promise<any>{
        return await this.aspiranteService.listaAspirantes();
    }

    @Post('acutualizar')
    async actualizarInformacion(@Body('aspirante') aspirante:any):Promise<any>{
        if(!aspirante){
            return {response:{error:true,message:"JSON empty"}}
        }
        return await this.aspiranteService.actualizarInformacion(aspirante);
    }

    @Post('eliminar')
    async eliminarAspirante(@Body('aspirante')aspirante:any):Promise<any>{
        return await this.aspiranteService.eliminar(aspirante)
    }
 
}
