import { Controller, Post, Body, Get } from '@nestjs/common';
import { ReportesService } from './reportes.service';

@Controller('reportes')
export class ReportesController {

    constructor(private reportesService:ReportesService){}


    @Post('registro')
    async registro(@Body('reporte') reporte:any):Promise<any>{
        if(!reporte){
            return{response:{error:true,message:"JSON empty"}}
        }
        return await this.reportesService.registro(reporte)
    }

    @Get('lista')
    async lista():Promise<any>{
        return await this.reportesService.lista()
    }

    @Post('eliminar')
    async eliminar(@Body('reportes') reportes:any):Promise<any>{
        return await this.reportesService.eliminar(reportes)
    }
}
