import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { type } from "os";
import { AspiranteEntity } from "../../aspirantse/AspiranteEntity/aspiranteBean.entity";
import { CarreraEntity } from "../../carrera/CarreraEntity/carreraBean.entity";
import { GrupoCarreraAspiranteEntity } from "../../grupo-carrera-aspirante/GrupoCarreraAspiranteEntity/grupoCarreraAspirante.entity";
import { GrupoCareraEntity } from "../../grupo-carrera/GrupoCarreraEntity/grupoCarreraBean.entity";
import { ImparteMateriasEntity } from "../../imparte-materias/imparteMateriasEntity/imparteMateriasBean.entity";
import { HorarioEntity } from "../../horario/HorarioEntity/horarioBean.entity";


@Entity('grupo_profesor')
export class GrupoProfesorEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type=>GrupoCareraEntity,grupoCarreraGrupoProfesor=>grupoCarreraGrupoProfesor.grupoProfesorGrupoCarrera)
  grupoCarreraGrupoProfesor:GrupoCareraEntity

  @ManyToOne(type=>ImparteMateriasEntity,imparteMateriasGrupoProfesor=>imparteMateriasGrupoProfesor)
  imparteMateriasGrupoProfesor:ImparteMateriasEntity

  @ManyToOne(type=>HorarioEntity,horarioGrupoPofesor=>horarioGrupoPofesor.grupoProfesorHorario)
  horarioGrupoPofesor:HorarioEntity

}
