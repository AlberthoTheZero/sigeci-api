import { Test, TestingModule } from '@nestjs/testing';
import { GrupoProfesorService } from './grupo-profesor.service';

describe('GrupoProfesorService', () => {
  let service: GrupoProfesorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GrupoProfesorService],
    }).compile();

    service = module.get<GrupoProfesorService>(GrupoProfesorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
