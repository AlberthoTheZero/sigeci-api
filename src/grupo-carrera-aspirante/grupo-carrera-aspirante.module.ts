import { Module } from '@nestjs/common';
import { GrupoCarreraAspiranteController } from './grupo-carrera-aspirante.controller';
import { GrupoCarreraAspiranteService } from './grupo-carrera-aspirante.service';
import { GrupoCarreraAspiranteEntity } from './GrupoCarreraAspiranteEntity/grupoCarreraAspirante.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([GrupoCarreraAspiranteEntity])],
  controllers: [GrupoCarreraAspiranteController],
  providers: [GrupoCarreraAspiranteService]
})
export class GrupoCarreraAspiranteModule {}
