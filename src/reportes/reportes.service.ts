import { Injectable } from '@nestjs/common';
import { ReportesEntity } from './ReportesEntity/reportesBean.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ReportesService {

    constructor(
        @InjectRepository(ReportesEntity)
        private readonly reportesRepository: Repository<ReportesEntity>
    ) { }

    async registro(reporte: any): Promise<any> {
        try {
            let sqlInsertarReporte = "insert into reportes values(null,'" + reporte.observacion + "','" + reporte.imagenURL + "'," + reporte.idUsuario + ");"
            await this.reportesRepository.query(sqlInsertarReporte);
            return { response: { error: false, message: "Reporte enviado" } }
        } catch (err) {
            return { response: { error: true, message: "JSON mal formado", messageError: err } }
        }

    }

    async lista(){
        return await this.reportesRepository.query("select * from reportes order by id desc;")
    }

    async eliminar(reportes:any):Promise<any>{
        try {
            let sqlEliminarReporte="DELETE FROM reportes WHERE id = '"+reportes.id+"';"
            await this.reportesRepository.query(sqlEliminarReporte)
            return {response:{error:false, message:"Reporte eliminado correctamente"}}
        } catch (err) {
            return {response:{error:true, message:"Error al eliminar el reporte", messageError:err}}
        }
    }
}
