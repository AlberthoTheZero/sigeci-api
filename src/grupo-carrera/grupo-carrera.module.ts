import { Module } from '@nestjs/common';
import { GrupoCarreraController } from './grupo-carrera.controller';
import { GrupoCarreraService } from './grupo-carrera.service';
import { GrupoCareraEntity } from './GrupoCarreraEntity/grupoCarreraBean.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([GrupoCareraEntity])],
  controllers: [GrupoCarreraController],
  providers: [GrupoCarreraService]
})
export class GrupoCarreraModule {}
