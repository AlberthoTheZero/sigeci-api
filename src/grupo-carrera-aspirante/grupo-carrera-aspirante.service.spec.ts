import { Test, TestingModule } from '@nestjs/testing';
import { GrupoCarreraAspiranteService } from './grupo-carrera-aspirante.service';

describe('GrupoCarreraAspiranteService', () => {
  let service: GrupoCarreraAspiranteService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GrupoCarreraAspiranteService],
    }).compile();

    service = module.get<GrupoCarreraAspiranteService>(GrupoCarreraAspiranteService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
