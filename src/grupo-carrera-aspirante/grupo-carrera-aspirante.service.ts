import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GrupoCarreraAspiranteEntity } from './GrupoCarreraAspiranteEntity/grupoCarreraAspirante.entity';
import { Repository } from 'typeorm';

@Injectable()
export class GrupoCarreraAspiranteService {
    constructor(
        @InjectRepository(GrupoCarreraAspiranteEntity)
        private readonly grupoCarreraAspiranteRepository: Repository<GrupoCarreraAspiranteEntity>
    ) { }

    async listasParaRegistro(carrera: any): Promise<any> {
        try {
            let sqlGruposDisponiblesCarrera = "select g.id,gradoGrupo,letraGrupo,nombreCarrera,"
                + "acronimoCarrera from grupo_carrera as gc join grupo as g on g.id=gc.grupoConCarreraId "
                + "join carrera as c on gc.asignaGrupoCarreraId=c.id WHERE c.id=" + carrera.id + ";";
       
                let sqlAlumnosCarreraSinRegistrar = "select a.id,a.primerApellido,a.segundoApellido,a.curp,c.nombreCarrera from aspirante as a left join grupo_carrera_aspirante as gca on gca.aspiranteId=a.id join carrera as c on c.id=a.carreraAspiranteId where gca.id is null and c.id='" + carrera.id + "';"
              let listaGrupos=  await this.grupoCarreraAspiranteRepository.query(sqlAlumnosCarreraSinRegistrar)
              let listaAlumnos=  await this.grupoCarreraAspiranteRepository.query(sqlGruposDisponiblesCarrera)
       return {response:{error:false,message:"Las consultas se realizon",listaAlumnos:listaAlumnos,listaGrupos:listaGrupos}}
        } catch (err) {
            return { response: { error: true,message:"Error al ejecutar", errorMessage: err } }
        }
    }


    async registroGrupoCarreraAspirante(registro: any): Promise<any> {
        try {
            let existGrupoCarreraAspirante: any[] = await this.grupoCarreraAspiranteRepository.query("select * from grupo_carrera_aspirante where grupoCarreraId = '" + registro.grupoCarreraID + "' and aspiranteId = '" + registro.aspiranteId + "';")
            if (existGrupoCarreraAspirante.length > 0) {
                return { response: { error: true, message: "Ya existe éste registro" } }
            } else {
                await this.grupoCarreraAspiranteRepository.query("insert into grupo_carrera_aspirante values(null,'" + registro.grupoCarreraID + "','" + registro.aspiranteId + "');")
                return { response: { error: false, message: "Registro exitoso." } }
            }
        } catch (err) {
            return { response: { error: true, message: "Error al registrar", errorMessage: err } }
        }
    }

    async modificarGrupoCarreraAspirante(modificar: any): Promise<any> {
        try {
            let existGrupoCarreraAspirante: any[] = await this.grupoCarreraAspiranteRepository.query("select * from grupo_carrera_aspirante where grupoCarreraId = '" + modificar.grupoCarreraID + "' and aspiranteId = '" + modificar.aspiranteId + "';")
            if (existGrupoCarreraAspirante.length > 0) {
                return { response: { error: true, message: "Ya existe el registro" } }
            } else {
                await this.grupoCarreraAspiranteRepository.query("UPDATE grupo_carrera_aspirante SET grupoCarreraID = '"+modificar.grupoCarreraID+"', aspiranteId = '"+modificar.aspiranteId+"' WHERE id = '"+modificar.id+"';")
                return { response: { error: false, message: "Modificación exitosa." } }
            }
        } catch (err) {
            return { response: { error: true, message: "Error al modificar.", errorMessage: err } }
        }
    }

    async lista():Promise<any>{
        try{
            return await this.grupoCarreraAspiranteRepository.query("select gca.id,g.id as idGrupo,g.gradoGrupo,g.letraGrupo,c.id as idCarrera,c.nombreCarrera,c.acronimoCarrera,a.id as idAspirante,a.nombre,primerApellido,a.segundoApellido from grupo_carrera_aspirante as gca join aspirante as a on a.id=gca.aspiranteId join grupo as g on g.id=gca.grupoCarreraID join carrera as c on c.id=a.carreraAspiranteId;")
        }catch(err){
            return {response:{error:true,message:"Error al consultar",errorMessage:err}}
        }
    }

    async eliminarGrupoCarreraAspirante(grupo_carrera_aspirante:any):Promise<any>{
        try {
          let sqlElminarGrupoCarreraAspirante="delete from grupo_carrera_aspirante where id = '"+grupo_carrera_aspirante.id+"';"
          await this.grupoCarreraAspiranteRepository.query(sqlElminarGrupoCarreraAspirante)
          return {response:{error:false, message:"La unión de grupo, carrera y aspirante ha sido eliminado correctamente"}}
        } catch (err) {
          return {response:{error:true, message:"Error al eliminar la unión grupo, carrera y aspirante", errorMessage:err}}
        }
      }

}
