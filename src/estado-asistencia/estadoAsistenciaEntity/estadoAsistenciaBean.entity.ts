import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { type } from "os";
import { GrupoHorarioEntity } from "../../grupo-horario/GrupoHorarioEntity/grupoHorarioBean.entity";

@Entity('estadoAsistencia')
export class EstadoAsistenciaEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  estado:string

   @OneToMany(type=>GrupoHorarioEntity,grupoHorarioAsistencia=>grupoHorarioAsistencia.estadoAsistenciaGrupoHorario)
   grupoHorarioAsistencia:GrupoHorarioEntity
}
