import { Module } from '@nestjs/common';
import { DivicionConCarreraController } from './divicion-con-carrera.controller';
import { DivicionConCarreraService } from './divicion-con-carrera.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DivicionConCarreraEntity } from './DivicionConCarreraEntity/divicionConCarreraBean.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DivicionConCarreraEntity])],
  controllers: [DivicionConCarreraController],
  providers: [DivicionConCarreraService]
})
export class DivicionConCarreraModule {}
