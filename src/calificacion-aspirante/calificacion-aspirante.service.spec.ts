import { Test, TestingModule } from '@nestjs/testing';
import { CalificacionAspiranteService } from './calificacion-aspirante.service';

describe('CalificacionAspiranteService', () => {
  let service: CalificacionAspiranteService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CalificacionAspiranteService],
    }).compile();

    service = module.get<CalificacionAspiranteService>(CalificacionAspiranteService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
