import { Entity, PrimaryGeneratedColumn, Column, Double, OneToMany, ManyToOne } from 'typeorm'
import { type } from 'os';
import { AspiranteEntity } from '../../aspirantse/AspiranteEntity/aspiranteBean.entity';
import { TrabajadorEntity } from '../../trabajador/trabajadorEntity/trabajadorBean.entity';
import { MateriasEntity } from '../../materias/MateriasEntity/materiaBean.entity';

@Entity('calificacionAspirante')
export class CalificacionAspiranteEntity {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    calificacion: number

    @ManyToOne(type => AspiranteEntity, id_aspirante => id_aspirante.calificacionId)
    id_aspirante: AspiranteEntity

    @ManyToOne(type=>TrabajadorEntity, id_trabajador=>id_trabajador.calificacionId)
    id_trabajador: TrabajadorEntity

    @ManyToOne(type=>MateriasEntity, id_materia=>id_materia.calificacion)
    id_materia: MateriasEntity
}