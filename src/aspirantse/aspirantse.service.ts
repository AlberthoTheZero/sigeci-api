import { Injectable, Get } from '@nestjs/common';
import { AspiranteEntity } from './AspiranteEntity/aspiranteBean.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AspirantseService {

    constructor(
        @InjectRepository(AspiranteEntity)
        private readonly userRepository: Repository<AspiranteEntity>
      ) {}



async registroAspirante(aspirante:any):Promise<any>{

  try{
    let existUSER=await this.userRepository.query("select * from aspirante where curp='"+aspirante.curp+"';");
    console.log("Busqueda->"+"select * from aspirante where curp='"+aspirante.curp+"';")
    console.log(JSON.stringify(existUSER))
    if(existUSER[0].id>0){
       return {response:{error:true ,message:"Aspirante ya esta registrado"}}
    }

  }catch(err){

  }



  let SQLInsertarAspirante:string=""
    +"insert into aspirante values("+
     " null,"+
      "'"+aspirante.nombre+"',"+
     "'"+aspirante.primerApellido+"',"+
     " '"+aspirante.segundoApellido+"',"+
      "'"+aspirante.curp+"',"+
      "'"+aspirante.fechaNacimiento+"',"+
      "'"+aspirante.nombreCalle+"',"+
      "'"+aspirante.numeroCasa+"',"+
      "'"+aspirante.colonia+"',"+
      "'"+aspirante.estado+"',"+
      "'"+aspirante.municipio+"',"+
      aspirante.codPostal+","+
      "'"+aspirante.telCasa+"',"+
      "'"+aspirante.telMovil+"',"+
      "'"+aspirante.correoPersonal+"',"+
      "'"+aspirante.sexo+"',"+
      aspirante.carreraID+
      ");";

  try{
    console.log("Salida del QUERY \n"+SQLInsertarAspirante)
    await this.userRepository.query(SQLInsertarAspirante)
  }catch(err){
    return {response:{error:true,errorMessage:err,message:"Error to insert ASPIRANTE in TABLE ASPIRANTE=> JSON =>"+JSON.stringify(aspirante)}}
  }

  let clave=JSON.stringify(aspirante.curp)
  let SQLInsertarUsuarioAlumno="insert into usuario values("+
   " null,"+
    "'"+aspirante.curp+"',"+
    "'"+clave.substring(clave.length-4,clave.length-1)+"',"+
    "1,4"+
    ");";

  try{
    await this.userRepository.query(SQLInsertarUsuarioAlumno);
  }catch(err){
    return {response:{error:true,errorMessage:err,message:"Error to insert ASPIRANTE in TABLE USUARIO=> JSON =>"+JSON.stringify(aspirante)}}
  }
return {response:{error:false,message:"Aspirante Registrado correctamente"}}
}




async listaAspirantes():Promise<any>{
return await this.userRepository.query('select a.id,nombre,primerApellido,segundoApellido,a.correoPersonal,c.nombreCarrera,c.acronimoCarrera from aspirante as a join carrera as c on c.id=a.carreraAspiranteId;')
}

async actualizarInformacion(aspirante:any):Promise<any>{
  try{
  let sqlUpdateAspirante="update aspirante "+ 
"set nombre='"+aspirante.nuevoNombre+"',"+
"primerApellido='"+aspirante.nuevoPrimerApellido+"',"+
"segundoApellido='"+aspirante.nuevoSegundoApellido+"',"+
"fechaNacimiento='"+aspirante.nuevaFechaNacimiento+"',"+
"calle='"+aspirante.nuevaCalle+"',"+
"numeroCasa="+aspirante.nuevoNumeroCasa+","+
"colonia='"+aspirante.nuevaColonia+"',"+
"estado='"+aspirante.nuevoEstado+"',"+
"municipio='"+aspirante.nuevoMunicipio+"',"+
"codigoPostal="+aspirante.nuevoCodigoPostal+","+
"telefonoCasa='"+aspirante.nuevoTelefonoCasa+"',"+
"telefonoMovil='"+aspirante.nuevoTelefonoMovil+"',"+
"correoPersonal='"+aspirante.nuevoCorreoPersonal+"',"+
"sexo='"+aspirante.nuevoSexo+"' "+
 "where id="+aspirante.idAspirante+";";


 
await this.userRepository.query(sqlUpdateAspirante)
return {response:{error:false,message:"La información del usuario se actualizo"}}

 }catch(err){
  console.log(err)
  return {response:{error:true,message:"Error OBJECT",messageError:err}}
 }
}

async eliminar(aspirante:any):Promise<any>{
  try {
    let sqlEliminarAspirante="DELETE FROM aspirante WHERE id = '"+aspirante.id+"';"
    await this.userRepository.query(sqlEliminarAspirante)
    return {response:{error:false, message:"Aspirante eliminado correctamente"}}
  } catch (err) {
    return {response:{error:true, message:"Error al eliminar aspirante", errorMessage:err}}
  }
}
}
 
