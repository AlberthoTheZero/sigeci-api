import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { TrabajadorModule } from './trabajador/trabajador.module';
import { UsuarioModule } from './usuario/usuario.module';
import { EvaluacionModule } from './evaluacion/evaluacion.module';
import { PreguntasModule } from './preguntas/preguntas.module';
import { AspiranteModule } from './aspirantse/aspirante.module';
import { RolModule } from './rol/rol.module';
import { DivicionAcademicaModule } from './divicion-academica/divicion-academica.module';
import { CarreraModule } from './carrera/carrera.module';
import { GrupoModule } from './grupo/grupo.module';
import { EstadoAsistenciaModule } from './estado-asistencia/estado-asistencia.module';
import { MateriasModule } from './materias/materias.module';
import { MateriaCarreraModule } from './materia-carrera/materia-carrera.module';
import { HorarioModule } from './horario/horario.module';
import { TiempoModule } from './tiempo/tiempo.module';
import { ImparteMateriasModule } from './imparte-materias/imparte-materias.module';
import { DivicionConCarreraModule } from './divicion-con-carrera/divicion-con-carrera.module';
import { GrupoCarreraModule } from './grupo-carrera/grupo-carrera.module';
import { GrupoCarreraAspiranteModule } from './grupo-carrera-aspirante/grupo-carrera-aspirante.module';
import { GrupoProfesorModule } from './grupo-profesor/grupo-profesor.module';
import { GrupoHorarioModule } from './grupo-horario/grupo-horario.module';
import { ReportesModule } from './reportes/reportes.module';
import { CalificacionAspiranteModule } from './calificacion-aspirante/calificacion-aspirante.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    UserModule,
    TrabajadorModule,
    UsuarioModule,
    EvaluacionModule,
    AspiranteModule,
    PreguntasModule,
    RolModule,
    DivicionAcademicaModule,
    CarreraModule,
    GrupoModule,
    EstadoAsistenciaModule,
    MateriasModule,
    MateriaCarreraModule,
    HorarioModule,
    TiempoModule,
    ImparteMateriasModule,
    DivicionConCarreraModule,
    GrupoCarreraModule,
    GrupoCarreraAspiranteModule,
    GrupoProfesorModule,
    GrupoHorarioModule,
    ReportesModule,
    CalificacionAspiranteModule,
  ],
  controllers: [
    AppController,
    
  ],
  providers: []
})
export class ApplicationModule {
  constructor(private readonly connection: Connection) {}
}
