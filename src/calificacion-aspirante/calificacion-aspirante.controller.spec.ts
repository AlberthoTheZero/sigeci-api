import { Test, TestingModule } from '@nestjs/testing';
import { CalificacionAspiranteController } from './calificacion-aspirante.controller';

describe('CalificacionAspirante Controller', () => {
  let controller: CalificacionAspiranteController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CalificacionAspiranteController],
    }).compile();

    controller = module.get<CalificacionAspiranteController>(CalificacionAspiranteController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
