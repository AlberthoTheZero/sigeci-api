import { Controller, Get, Post, Body } from '@nestjs/common';
import { PreguntaEntity } from './PreguntasEntity/preguntasBean.entity';
import { PreguntasService } from './preguntas.service';

@Controller('preguntas')
export class PreguntasController {

    constructor(private preguntaService:PreguntasService){}

    @Get('vistaAspirante')
    async vistaAspirante(): Promise<any> {
        return await this.preguntaService.vistaAspirante()
    }

    @Get('vistaServicios')
    async vistaServicios(): Promise<any> {
        return await this.preguntaService.vistaServiciosEscoles()
    }

    @Post('registro')
    async registro(@Body('registro') registro: any): Promise<any> {
        if (!registro) {
            return { response: { error: true, message: "JSON vacio" } }
        }
        return await this.preguntaService.registro(registro);
    }

    @Post('modificar')
    async modificar(@Body('modificar') modificar: any): Promise<any> {
        if (!modificar) {
            return { response: { error: true, message: "JSON vacio" } }
        }
        return await this.preguntaService.modificar(modificar);
    }

    @Post('eliminar')
    async eliminar(@Body('eliminar') eliminar:any): Promise<any> {
        if (!eliminar) {
            return { response: { error: true, message: "JSON vacio" } }
        }
        return await this.preguntaService.eliminar(eliminar);
    }

    @Post('activar')
    async activar(@Body('activar') activar:any): Promise<any> {
        if (!activar) {
            return { response: { error: true, message: "JSON vacio" } }
        }
        return await this.preguntaService.activar(activar);
    }

    @Post('recuperar')
    async recuperar(@Body('recuperar') recuperar:any): Promise<any> {
        if (!recuperar) {
            return { response: { error: true, message: "JSON vacio" } }
        }
        return await this.preguntaService.recuperar(recuperar);
    }

}
