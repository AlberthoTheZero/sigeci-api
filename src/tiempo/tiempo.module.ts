import { Module } from '@nestjs/common';
import { TiempoController } from './tiempo.controller';
import { TiempoService } from './tiempo.service';

@Module({
  controllers: [TiempoController],
  providers: [TiempoService]
})
export class TiempoModule {


  
}
