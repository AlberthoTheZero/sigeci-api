import { Controller, Post, Body } from '@nestjs/common';
import { GrupoHorarioService } from './grupo-horario.service';

@Controller('grupo-horario')
export class GrupoHorarioController {
    constructor(private grupoHorarioService:GrupoHorarioService){}

    
    @Post('horarios')
    async horarios(@Body('docente') docente:any):Promise<any>{
        if(!docente){
            return {response:{error:true,message:"JSON vacio"}}
        }
        return await this.grupoHorarioService.horarioTrabajador(docente)
    }

    @Post('listaAsistencia')
    async lista(@Body('docente') docente:any):Promise<any>{
        if(!docente){
            return {response:{error:true,message:"JSON vacio"}}
        }
return await this.grupoHorarioService.listaAsistencia(docente);
    }


    @Post('registro')
    async registro(@Body('registro') registro:any):Promise<any>{
        if(!registro){
            return {response:{error:true,message:"JSON vacio"}}
        }
        return await this.grupoHorarioService.registro(registro)
    }
}
