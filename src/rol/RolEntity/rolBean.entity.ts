import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { UserEntity } from "../../user/user.entity";
import { UsuarioEntity } from "../../usuario/UsuarioEntity/usuarioBean.entity";
import { type } from "os";


@Entity('rol')
export class RolEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombreRol:string

  @OneToMany(type=>UsuarioEntity,usuarioRol=>usuarioRol.rolUsuario)
  usuarioRol:UsuarioEntity


}
