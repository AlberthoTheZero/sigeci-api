import { Injectable } from '@nestjs/common';
import { MateriaCarreraEntity } from './MateriaCarreraEntity/materiaCarreraBean.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { async } from 'rxjs/internal/scheduler/async';

@Injectable()
export class MateriaCarreraService {
    constructor(
        @InjectRepository(MateriaCarreraEntity)
        private readonly materiaCarreraRepository: Repository<MateriaCarreraEntity>
    ) { }


      async listasParaRegistro(division:any){
        try{
            let listaMaterias=await this.materiaCarreraRepository.query("select m.id,m.nombreMateria from materias as m join impartematerias as im on im.materiasImparteId=m.id join trabajador as t on t.id=im.trabajadorImparteId where t.divicionTrabajadorId="+division.id+";");
            let listaCarreras=await this.materiaCarreraRepository.query("select c.id,c.nombreCarrera,c.acronimoCarrera from carrera as c join divicion_carrera as dc on dc.carreraConDivicionId=c.id where dc.divicionConCarreraId="+division.id+";")
            return {response:{error:false,listaCarreras:listaCarreras,listaMaterias:listaMaterias}}
              
        }catch(err){
            return {response:{error:true,message:"Fallo la consulta",errorMessage:err}}

        } 
    }
    
    async registro(registro:any):Promise<any>{
        try{
            let siExiste:any[]= await this.materiaCarreraRepository.query("select * from materiacarrera where materiasAsignaCarreraId="+registro.idMateria+" and carreraAsignaMateriasId="+registro.idCarera+";")
            if(siExiste.length>0){
                return {response:{error:true,message:"El registro ya existe"}} 
            }else{
                
                    let verificarDos= await this.materiaCarreraRepository.query("select * from materiacarrera as m  where m.carreraAsignaMateriasId="+registro.idCarera+";")
                    if(verificarDos.length>=2){
                        return {response:{error:true,message:"Ya hay dos regsitros"}}
                    }else{
                        await this.materiaCarreraRepository.query("insert into materiacarrera values(null,"+registro.idMateria+","+registro.idCarera+");")
                        return {response:{error:false,message:"Se registro correctamente"}}
                    }
            }
        } catch (err) {
            return { response: { error: true, message: "Fallo la consulta", errorMessage: err } }
        }
    }

    async modificar(modificar: any): Promise<any> {
        try {
            let siExiste: any[] = await this.materiaCarreraRepository.query("select * from materiacarrera where materiasAsignaCarreraId=" + modificar.idMateria + " and carreraAsignaMateriasId=" + modificar.idCarera + ";")
            if (siExiste.length > 0) {
                return { response: { error: true, message: "El registro ya existe" } }
            } else {
                await this.materiaCarreraRepository.query("update materiacarrera set materiasAsignaCarreraId=" + modificar.idMateria + ",carreraAsignaMateriasId=" + modificar.idCarera + " where id=" + modificar.id + ";")
            }
        } catch (err) {
            return { response: { error: true, message: "Fallo la consulta", errorMessage: err } }
        }
    }
    
    async lista(division:any):Promise<any>{
        return await this.materiaCarreraRepository.query("select mc.id,c.id as idCarrera,nombreCarrera,m.id as idMateria,nombreMateria from materiacarrera as mc join materias as m on mc.materiasAsignaCarreraId=m.id join carrera as c on c.id=mc.carreraAsignaMateriasId join divicion_carrera as dc on dc.carreraConDivicionId=c.id where dc.divicionConCarreraId="+division.id+";")
    }

    async eliminar(materiacarrera: any): Promise<any> {
        try {
            let sqlEliminarMateriaCarrera = "DELETE FROM materiacarrera WHERE id ='" + materiacarrera.id + "';"
            await this.materiaCarreraRepository.query(sqlEliminarMateriaCarrera)
            return { response: { error: false, message: "Materia carrera eliminado correctamente" } }
        } catch (err) {
            return { response: { error: true, message: "Error al eliminar materia carrera", errorMessage: err } }
        }
    }
}
