import { Controller, Get, Post, Body } from '@nestjs/common';
import { GrupoCarreraAspiranteService } from '../grupo-carrera-aspirante/grupo-carrera-aspirante.service';
import { GrupoCareraEntity } from './GrupoCarreraEntity/grupoCarreraBean.entity';
import { GrupoCarreraService } from './grupo-carrera.service';

@Controller('grupo-carrera')
export class GrupoCarreraController {

constructor(private grupoCarreraService:GrupoCarreraService){}


@Get('listasParaRegistro')
async listasParaRegistro():Promise<any>{
    return await this.grupoCarreraService.listasParaRegistro();
}

@Post('registro')
async registro(@Body('registro') registro:any):Promise<any>{
    if(!registro){
        return {response:{error:true,message:"JSON vacio"}}
    }
    return await this.grupoCarreraService.registro(registro);
}

@Post('modificar')
async modificar(@Body('modificar') modificar:any):Promise<any>{
if(!modificar){
    return {response:{error:true,message:"JSON vacio"}}
}
return await this.grupoCarreraService.modificar(modificar)
}

@Get('lista')
async lista(){
    return await this.grupoCarreraService.lista()
}

@Post('eliminar')
async eliminar(@Body('grupoCarrera') grupoCarrera: any):Promise<any>{
    return await this.grupoCarreraService.eliminar(grupoCarrera)
}


}
