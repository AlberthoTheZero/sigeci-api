import { Test, TestingModule } from '@nestjs/testing';
import { MateriaCarreraService } from './materia-carrera.service';

describe('MateriaCarreraService', () => {
  let service: MateriaCarreraService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MateriaCarreraService],
    }).compile();

    service = module.get<MateriaCarreraService>(MateriaCarreraService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
