import { Controller, Post, Body, Get } from '@nestjs/common';
import { TrabajadorService } from './trabajador.service';

@Controller('trabajador')
export class TrabajadorController {

    constructor(private trabajadorService: TrabajadorService) {

    }

    @Post('registro')
    async registroTrabajador(@Body('trabajador') trabajador: any): Promise<any> {
        return await this.trabajadorService.registroTrabajador(trabajador)
    }

    @Get('listas')
    async listas(): Promise<any> {
        return await this.trabajadorService.listasRegistro();
    }

    @Post('modificacion')
    async modificarTrabajador(@Body('trabajador') trabajador: any): Promise<any> {
        return await this.trabajadorService.modificarTrabajador(trabajador)
    }
    @Post('modificacionDivision')
    async modificarTrabajadorDivision(@Body('trabajador') trabajador: any): Promise<any> {
        return await this.trabajadorService.modificarTrabajadorDivision(trabajador)
    }

    @Get('listaDirectores')
    async listaDirectores(): Promise<any> {
        return this.trabajadorService.listaDirectores()
    }

    @Post('listaDocentes')
    async listaDocentes(@Body('divicion') divicion: any): Promise<any> {
        return this.trabajadorService.listaDocentes(divicion)
    }

    @Post('eliminar')
    async eliminarTrabajador(@Body('eliminar') eliminar: any): Promise<any> {
        return this.trabajadorService.eliminarTrabajador(eliminar)
    }

}
