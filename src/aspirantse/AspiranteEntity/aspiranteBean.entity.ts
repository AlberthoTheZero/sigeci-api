import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { type } from "os";
import { EvaluacionEntity } from "../../evaluacion/EvaluacioinEntity/evaluacionBean.entity";
import { CarreraEntity } from "../../carrera/CarreraEntity/carreraBean.entity";
import { CalificacionAspiranteEntity } from "../../calificacion-aspirante/CalificacionAspiranteEntity/calificacionAspiranteBean.entity";
import { GrupoHorarioEntity } from "../../grupo-horario/GrupoHorarioEntity/grupoHorarioBean.entity";


@Entity('aspirante')
export class AspiranteEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombre:string

  @Column()
  primerApellido:string

  @Column()
  segundoApellido:string

  @Column()
  curp:string

  @Column()
  fechaNacimiento:Date

  @Column()
  calle:string

  @Column()
  numeroCasa:number
  
  @Column()
  colonia:string

  @Column()
  estado:string

  @Column()
  municipio:string

  @Column()
  codigoPostal:number

  @Column()
  telefonoCasa:string

  @Column()
  telefonoMovil:string

  @Column()
  correoPersonal:string

  @Column()
  sexo:string

  @OneToMany(type=>EvaluacionEntity,evaluacionAspirante=>evaluacionAspirante.aspiranteEvaluacion)
  evaluacionAspirante:EvaluacionEntity

  @ManyToOne(type=>CarreraEntity,carreraAspirante=>carreraAspirante.aspiranteCarrera)
  carreraAspirante:CarreraEntity

  @OneToMany(type=>CalificacionAspiranteEntity, calificacionId=>calificacionId.id_aspirante)
  calificacionId : CalificacionAspiranteEntity
  
}
