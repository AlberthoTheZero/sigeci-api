import { Module } from '@nestjs/common';
import { ReportesController } from './reportes.controller';
import { ReportesService } from './reportes.service';
import { ReportesEntity } from './ReportesEntity/reportesBean.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([ReportesEntity])],
  controllers: [ReportesController],
  providers: [ReportesService]
})
export class ReportesModule {}
