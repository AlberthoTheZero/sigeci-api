import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsuarioEntity } from './UsuarioEntity/usuarioBean.entity';
import { Repository, LessThan } from 'typeorm';
import { userInfo } from 'os';

@Injectable()
export class UsuarioService {

    constructor(
        @InjectRepository(UsuarioEntity)
        private readonly userRepository: Repository<UsuarioEntity>
    ) { }



    async login(usuario: any): Promise<any> {

        if (!usuario) {
            return { response: { error: true, message: "Datas error object " + JSON.stringify(usuario) } }
        }

        let usuarioInfo = await this.userRepository.query("call sp_Login('" + usuario.usuario + "','" + usuario.clave + "');")
        console.log("JSON-USER->" + JSON.stringify(usuarioInfo))
        try {
            console.log("")
            console.log("Busca->" + usuarioInfo[0][0].rolUsuarioId)
            console.log("")

            switch (usuarioInfo[0][0].rolUsuarioId) {
                case 1:
                    usuarioInfo = await this.userRepository.query("SELECT * FROM trabajador as t join usuario as u on u.usuarioNombre=t.curp join rol as r on r.id=u.rolUsuarioId where usuarioNombre='" + usuario.usuario + "' and clave='" + usuario.clave + "';");
                    let listaDocentes = await this.userRepository.query("select t.* from trabajador as t join usuario as u on u.usuarioNombre=t.curp join rol as r on r.id=u.rolUsuarioId join tiempo as ti on ti.id=t.tiempoTrabajadorId WHERE u.rolUsuarioId=3;")
                    let listaDivicionesAcademicas = await this.userRepository.query("select * from divicionacademica;")
                    let listaCarreras = await this.userRepository.query("select * from carrera;")
                    let listaDirectores = await this.userRepository.query("select * from trabajador as t join usuario as u on u.usuarioNombre=t.curp join rol as r on r.id=u.rolUsuarioId join tiempo as ti on ti.id=t.tiempoTrabajadorId WHERE u.rolUsuarioId=2;")
                    return { response: { error: false, response: usuarioInfo, listaDocentes: listaDocentes, listaCarreras: listaCarreras, listaDivicionesAcademicas: listaDivicionesAcademicas, listaDirectores: listaDirectores } }
                    break;
                case 2:
                    usuarioInfo = await this.userRepository.query("SELECT t.*,u.rolUsuarioId,r.nombreRol FROM trabajador as t join usuario as u on u.usuarioNombre=t.curp join rol as r on r.id=u.rolUsuarioId where usuarioNombre='" + usuario.usuario + "' and clave='" + usuario.clave + "';");
                    return { response: { error: false, response: usuarioInfo } }

                    break;
                case 3:
                    usuarioInfo = await this.userRepository.query("SELECT t.*,u.rolUsuarioId,r.nombreRol FROM trabajador as t join usuario as u on u.usuarioNombre=t.curp join rol as r on r.id=u.rolUsuarioId where usuarioNombre='" + usuario.usuario + "' and clave='" + usuario.clave + "';");
                    return { response: { error: false, response: usuarioInfo } }
                    break;
                case 4:
                    usuarioInfo = await this.userRepository.query("select a.*,u.rolUsuarioId,r.nombreRol from aspirante as a join usuario as u on u.usuarioNombre=a.curp join rol as r on r.id=u.rolUsuarioId where usuarioNombre='" + usuario.usuario + "' and clave='" + usuario.clave + "';");
                    return { response: { error: false, response: usuarioInfo } }
                    break;
                    case 5:
                        usuarioInfo = await this.userRepository.query("SELECT t.*,u.rolUsuarioId,r.nombreRol FROM trabajador as t join usuario as u on u.usuarioNombre=t.curp join rol as r on r.id=u.rolUsuarioId where usuarioNombre='" + usuario.usuario + "' and clave='" + usuario.clave + "';");
                        return { response: { error: false, response: usuarioInfo} }
                    break;

            }
            console.log("Salida->" + JSON.stringify(usuarioInfo))
            return { response: { error: false, response: usuarioInfo } }

        } catch (err) {

            return { response: { error: true, response: "User not found" } }
        }
    }


    async modificarClave(usuario: any): Promise<any> {
        try {
            let sqlCambioClaveUsuario = "UPDATE usuario SET clave = '"+usuario.clave+"' WHERE id='"+usuario.id+"';"
            await this.userRepository.query(sqlCambioClaveUsuario)
            return { response: { error: false, message: "Cambio de clave realizado correctamente." } }
        } catch (err) {
            return { response: { error: true, mmessage: "Error al cambiar clave.", errorMessage: err } }
        }
    }





}
