import { Module } from '@nestjs/common';
import { DivicionAcademicaController } from './divicion-academica.controller';
import { DivicionAcademicaService } from './divicion-academica.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DivicionAcademicaEntity } from './DivicionAcademicaEntity/divicionAcademicaBean.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DivicionAcademicaEntity])],
  controllers: [DivicionAcademicaController],
  providers: [DivicionAcademicaService]
})
export class DivicionAcademicaModule {}
