import { Test, TestingModule } from '@nestjs/testing';
import { EstadoAsistenciaService } from './estado-asistencia.service';

describe('EstadoAsistenciaService', () => {
  let service: EstadoAsistenciaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EstadoAsistenciaService],
    }).compile();

    service = module.get<EstadoAsistenciaService>(EstadoAsistenciaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
