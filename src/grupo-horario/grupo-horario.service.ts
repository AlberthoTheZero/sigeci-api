import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GrupoHorarioEntity } from './GrupoHorarioEntity/grupoHorarioBean.entity';
import { Repository } from 'typeorm';

@Injectable()
export class GrupoHorarioService {

    constructor(
        @InjectRepository(GrupoHorarioEntity)
        private readonly grupoHorarioRepository: Repository<GrupoHorarioEntity>
      ) { }



    async horarioTrabajador(docente:any):Promise<any>{
      try{
        let lista=await this.grupoHorarioRepository.query("select h.id,h.horaInicio,h.horaFin from trabajador as t join impartematerias as im on im.trabajadorImparteId=t.id join grupo_profesor as gp on gp.imparteMateriasGrupoProfesorId=im.id join horario as h on h.id=gp.horarioGrupoPofesorId where im.trabajadorImparteId="+docente.id+";")
        let listaEstadoAsistencia= await this.grupoHorarioRepository.query("select * from estadoasistencia;")
        return {response:{error:false,message:"Consulta Exitosa",listaHorario:lista,listaEstadoAsistencia:listaEstadoAsistencia}}
      }catch(err){
       return{response:{error:true,message:"Error interno",errorMessage:err}}
      }
    }


    async listaAsistencia(docente:any):Promise<any>{
        try{
          let horaAntes:any[]=await this.grupoHorarioRepository.query("select if(date_format(now(),'%H:%i')>'"+docente.hora+"',1,0) as tiempo;")
          if(horaAntes[0].tiempo==0){
            return {response:{error:null,message:"Aun no es hora de pasar lista",listaAspirantes:[]}}
          }
        }catch(err){
          return {response:{error:true,message:"Error al buscar si existian",errorMessage:err}}
        }

      try{
        let formato="'%Y-%m-%d'"
        let sqlBUSCAR="select a.id,a.nombre,a.primerApellido,a.segundoApellido,a.correoPersonal,gh.estadoAsistenciaGrupoHorarioId from grupo_horario as gh join aspirante as a on a.id=gh.aspiranteId where gh.fecha>=date_format(now(),"+formato+") and gh.fecha<=DATE_ADD(date_format(now(),"+formato+"),interval 1 day) and gh.horarioGrupoHorarioId="+docente.idHora+";"
       console.log(sqlBUSCAR)
        let busqueda:any[]= await this.grupoHorarioRepository.query(sqlBUSCAR)
       if(busqueda.length>0){
         return{response:true,message:"Lista recuperada",listaAspirantes:busqueda}
       }
      }catch(err){
          return {response:{error:true,message:"Error al buscar si existian",errorMessage:err}}
      }

      try{
        let activoReistro:any[]= await this.grupoHorarioRepository.query("select if(date_format(now(),'%H:%i')>='"+docente.hora+"' and date_format(now(),'%H:%i')<=date_format(addtime('1999-02-01 "+docente.hora+"','00:20'),'%H:%i') ,1,0) as tiempo;")
        console.log(JSON.stringify("Activo->"+activoReistro))
        if(activoReistro[0].tiempo==0){

          try{
            let listaFalta= await this.grupoHorarioRepository.query("select a.id,a.nombre,a.primerApellido,a.segundoApellido,a.correoPersonal,'0' as estado from grupo_carrera_aspirante as gpa join aspirante as a on gpa.aspiranteId=a.id join grupo_carrera as gc on gc.id=gpa.grupoCarreraID join grupo as g on g.id=gc.grupoConCarreraId join grupo_profesor as gp on gp.grupoCarreraGrupoProfesorId=gc.id join impartematerias as im on im.id=gp.imparteMateriasGrupoProfesorId join trabajador as t on t.id= im.trabajadorImparteId join horario as h on h.id=gp.horarioGrupoPofesorId where t.id="+docente.idDocente+" and h.id="+docente.idHora+";")
            console.log("Falta->"+JSON.stringify(listaFalta))
            for(let aspirante of listaFalta){
                await this.grupoHorarioRepository.query("insert into grupo_horario values(null,now(),"+aspirante.id+",2,"+docente.idHora+");")
            }
            return {response:{error:false,listaAspirantes: await this.grupoHorarioRepository.query("select gh.id,fecha,a.id as idAspirante,a.nombre,a.primerApellido,a.segundoApellido,a.correoPersonal,ea.id as idAsistencia,ea.estado,h.id as idHorario,h.horaInicio from grupo_horario as gh join aspirante as a on a.id=gh.aspiranteId join estadoasistencia as ea on ea.id=gh.estadoAsistenciaGrupoHorarioId join horario as h on h.id=gh.horarioGrupoHorarioId where h.id="+docente.idHora+";")
          }}
          }catch(err){
              return {response:{error:true,message:"Error al isertar despues de la hora",errorMessage:err}
          }
        }
          return {response:{error:true,message:"La hora ha expirado"}}
        } // Termino de la insecion
        let lista= await this.grupoHorarioRepository.query("select a.id,a.nombre,a.primerApellido,a.segundoApellido,a.correoPersonal,'0' as estado from grupo_carrera_aspirante as gpa join aspirante as a on gpa.aspiranteId=a.id join grupo_carrera as gc on gc.id=gpa.grupoCarreraID join grupo as g on g.id=gc.grupoConCarreraId join grupo_profesor as gp on gp.grupoCarreraGrupoProfesorId=gc.id join impartematerias as im on im.id=gp.imparteMateriasGrupoProfesorId join trabajador as t on t.id= im.trabajadorImparteId join horario as h on h.id=gp.horarioGrupoPofesorId where t.id="+docente.idDocente+" and h.id="+docente.idHora+";")
       return {response:{error:false,message:"Consulta Exitosa",listaAspirantes:lista}}
      }catch(err){
        return{response:{error:true,message:"Error interno",errorMessage:err}}
      }
    }

    async registro(registro:any):Promise<any>{
        try{
          let activoReistro:any[]= await this.grupoHorarioRepository.query("select if(date_format(now(),'%H:%i')>='"+registro.hora+"' and date_format(now(),'%H:%i')<=date_format(addtime('1999-02-01 "+registro.hora+"','00:20'),'%H:%i') ,1,0) as tiempo;")
          console.log(JSON.stringify("Activo->"+activoReistro))
          if(activoReistro[0].tiempo==0){
              for(let aspirante of registro.lista){
                if(aspirante.estado==3){
                    await this.grupoHorarioRepository.query("update grupo_horario set estadoAsistenciaGrupoHorarioId=3 where id="+aspirante.idAspirante+" and horarioGrupoHorarioId="+aspirante.idHora+" and fecha>='"+registro.fechaActual+"' and fecha<=DATE_ADD('"+registro.fechaActual+"',interval 1 day);")
                }else if(aspirante.estado==2){
                  await this.grupoHorarioRepository.query("insert into grupo_horario values(null,now(),"+aspirante.idAspirante+",2,"+aspirante.idHora+");")
                }else if(aspirante.estado==1){
                  await this.grupoHorarioRepository.query("insert into grupo_horario values(null,now(),"+aspirante.idAspirante+",1,"+aspirante.idHora+");")
                }
              }
          }
        }catch(err){
            return {response:{error:true,message:"Error al Insertar o Actualizar",errorMessage:err}}
        }
    }
     


}
