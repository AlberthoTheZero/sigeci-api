import { Module } from '@nestjs/common';
import { CalificacionAspiranteController } from './calificacion-aspirante.controller';
import { CalificacionAspiranteService } from './calificacion-aspirante.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CalificacionAspiranteEntity } from './CalificacionAspiranteEntity/calificacionAspiranteBean.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CalificacionAspiranteEntity])],
  controllers: [CalificacionAspiranteController],
  providers: [CalificacionAspiranteService]
})
export class CalificacionAspiranteModule {}
