import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DivicionConCarreraEntity } from './DivicionConCarreraEntity/divicionConCarreraBean.entity';
import { Repository } from 'typeorm';

@Injectable()
export class DivicionConCarreraService {
    
    constructor(
        @InjectRepository(DivicionConCarreraEntity)
        private readonly divicionCarraRepository: Repository<DivicionConCarreraEntity>
      ) {}

      async listasParaRegistro():Promise<any>{
        try{
            let listaDivisiones= await this.divicionCarraRepository.query("select * from divicionacademica;")
            let listaCarreras=await this.divicionCarraRepository.query("select * from carrera;")
        return {response:{error:false,message:"Consultas correctamente",listaDivisiones:listaDivisiones,listaCarreras:listaCarreras}}
        }catch(err){
            return {response:{error:true,message:"Consultas mal formadas",errorMessage:err}}
        }
      }


      async registro(registroEntrante:any):Promise<any>{
          try{
                let buscarRegistro:any[]=await this.divicionCarraRepository.query("select * from divicion_carrera where divicionConCarreraId="+registroEntrante.idDivision+" and carreraConDivicionId="+registroEntrante.idCarrera+";")
        if(buscarRegistro.length>0){
                  return {response:{error:true,message:"El registro ya exite"}}
              }else{
                    await this.divicionCarraRepository.query("insert into divicion_carrera values(null,"+registroEntrante.idDivision+","+registroEntrante.idCarrera+");")
                    return {response:{error:true,message:"Se registro correctamente"}}
                }
          }catch(err){
                console.log("entra al error->"+err)
                return {response:{error:true,message:"Error al registrar",errorMessage:err}}
          }

      }

      async lista():Promise<any>{
            try{
                let lista= await this.divicionCarraRepository.query("select dc.id,nombreDivicion,nombreCarrera,acronimoCarrera from divicion_carrera as dc join divicionacademica as d on dc.divicionConCarreraId=d.id join carrera as c on c.id=dc.carreraConDivicionId;")
                return {response:{error:false,message:"Consulta existosa",lista:lista}} 
            }catch(err){
                return {response:{error:true,message:"Fallo la consulta",errorMessage:err}}
            }
      }

      async modificar(modificar:any){
          try{
              let buscarExiste:any[]= await this.divicionCarraRepository.query("select * from divicion_carrera where divicionConCarreraId="+modificar.idDivision+" and carreraConDivicionId="+modificar.idCarrera+";")

            if(buscarExiste.length>0){
                return {response:{error:true,message:"Ya hay un registro con estos datos"}}
            }else{
                await this.divicionCarraRepository.query("update divicion_carrera set divicionConCarreraId="+modificar.idDivision+",carreraConDivicionId="+modificar.idCarrera+" where id="+modificar.id+";")
                return {response:{error:false,message:"Los datos se actualizaron"}}
            }
            
          }catch(err){
            return {response:{error:true,message:"Fallo la actualizacion",errorMessage:err}}
          }
      }

      async eliminar(eliminar:any){
          try{
            try{
                let buscarExiste:any[]= await this.divicionCarraRepository.query("select * from divicion_carrera where id="+eliminar.id+";")
              if(buscarExiste.length<=0){
                  return {response:{error:true,message:"No exite el registro que deceas eliminar"}}
              }else{
                  await this.divicionCarraRepository.query("delete from divicion_carrera where id="+eliminar.id+";")
                  return {response:{error:false,message:"Los datos se eliminaron"}}
              }
              
            }catch(err){
              return {response:{error:true,message:"No se puede eliminar esta Asignación",errorMessage:err}}
            }
            
          }catch{

          }
      }

}
