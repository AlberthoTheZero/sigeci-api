import { Controller, Get, Post, Body } from '@nestjs/common';
import { MateriaCarreraService } from './materia-carrera.service';

@Controller('materia-carrera')
export class MateriaCarreraController {

    constructor(private materiaCarreraService: MateriaCarreraService) { }


    @Post('listasParaRegistro')
    async listasParaRegistro(@Body('division') division:any) {
        return await this.materiaCarreraService.listasParaRegistro(division);
    }

    @Post('lista')
    async lista(@Body('division') division:any): Promise<any> {
        return await this.materiaCarreraService.lista(division);
    }

    @Post('registro')
    async registro(@Body('registro') registro: any): Promise<any> {
        if (!registro) {
            return { response: { error: true, message: "JSON vacio" } }
        }
        return this.materiaCarreraService.registro(registro)
    }

    @Post('modificar')
    async modificar(@Body('modificar') modificar: any): Promise<any> {
        if (!modificar) {
            return { response: { error: true, message: "JSON vacio" } }
            return this.materiaCarreraService.modificar(modificar)
        }
    }

    @Post('eliminar')
    async eliminarMateriaCarrera(@Body('materiacarrera') materiacarrera: any): Promise<any> {
        return await this.materiaCarreraService.eliminar(materiacarrera)
    }


}
