import { Test, TestingModule } from '@nestjs/testing';
import { EstadoAsistenciaController } from './estado-asistencia.controller';

describe('EstadoAsistencia Controller', () => {
  let controller: EstadoAsistenciaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EstadoAsistenciaController],
    }).compile();

    controller = module.get<EstadoAsistenciaController>(EstadoAsistenciaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
