import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { type } from "os";
import { GrupoEntity } from "../../grupo/GrupoEntity/grupoBean.entity";
import { CarreraEntity } from "../../carrera/CarreraEntity/carreraBean.entity";
import { GrupoCarreraAspiranteEntity } from "../../grupo-carrera-aspirante/GrupoCarreraAspiranteEntity/grupoCarreraAspirante.entity";
import { GrupoProfesorEntity } from "../../grupo-profesor/GrupoProfesorEntity/grupoProfesorBean.entity";

@Entity('grupo_carrera')
export class GrupoCareraEntity {

  @PrimaryGeneratedColumn()
  id: number;
  
  @ManyToOne(type=>GrupoEntity,grupoConCarrera=>grupoConCarrera.asignaGrupo)
  grupoConCarrera:GrupoEntity

  @ManyToOne(type=>CarreraEntity,asignaGrupoCarrera=>asignaGrupoCarrera.carreraConGrupo)
  asignaGrupoCarrera:CarreraEntity
  
  @OneToMany(type=>GrupoProfesorEntity,grupoProfesorGrupoCarrera=>grupoProfesorGrupoCarrera)
  grupoProfesorGrupoCarrera:GrupoProfesorEntity

  @OneToMany(type=>GrupoCarreraAspiranteEntity,carreraAspiranteId=>carreraAspiranteId)
  carreraAspiranteId:GrupoCarreraAspiranteEntity

  
 
}
