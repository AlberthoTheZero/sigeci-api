import { Test, TestingModule } from '@nestjs/testing';
import { GrupoCarreraService } from './grupo-carrera.service';

describe('GrupoCarreraService', () => {
  let service: GrupoCarreraService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GrupoCarreraService],
    }).compile();

    service = module.get<GrupoCarreraService>(GrupoCarreraService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
