import { Module } from '@nestjs/common';
import { PreguntasController } from './preguntas.controller';
import { PreguntasService } from './preguntas.service';
import { PreguntaEntity } from './PreguntasEntity/preguntasBean.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([PreguntaEntity])],
  controllers: [PreguntasController],
  providers: [PreguntasService]
})
export class PreguntasModule {}
