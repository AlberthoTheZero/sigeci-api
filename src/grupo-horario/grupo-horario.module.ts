import { Module } from '@nestjs/common';
import { GrupoHorarioController } from './grupo-horario.controller';
import { GrupoHorarioService } from './grupo-horario.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GrupoHorarioEntity } from './GrupoHorarioEntity/grupoHorarioBean.entity';

@Module({
  imports: [TypeOrmModule.forFeature([GrupoHorarioEntity])],
  controllers: [GrupoHorarioController],
  providers: [GrupoHorarioService]
})
export class GrupoHorarioModule {}
