import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ImparteMateriasEntity } from './imparteMateriasEntity/imparteMateriasBean.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ImparteMateriasService {

    constructor(
        @InjectRepository(ImparteMateriasEntity)
        private readonly imparteMateriasRepository: Repository<ImparteMateriasEntity>
      ) { } 

      async listaCarreras(division:any):Promise<any>{
        return await this.imparteMateriasRepository.query("select c.id,nombreCarrera from divicion_carrera as dc join divicionacademica as d on d.id=dc.divicionConCarreraId join carrera as c on dc.carreraConDivicionId=c.id where dc.divicionConCarreraId="+division.id+";")
      }

      async listasParaRegistrar(carrera:any){
        let listaProfesores= await this.imparteMateriasRepository.query("select t.* from trabajador as t join usuario as u on u.usuarioNombre=t.curp where u.rolUsuarioId=3 and t.divicionTrabajadorId="+carrera.id+";")
      return {response:{error:false,listaProfesores:listaProfesores}}
      }

      async listaMaterias(carrera:any):Promise<any>{
        let listaMaterias= await this.imparteMateriasRepository.query("select m.id,nombreMateria,nombreCarrera,acronimoCarrera from materias as m join materiacarrera as mc on mc.materiasAsignaCarreraId=m.id join carrera as c on c.id=mc.carreraAsignaMateriasId  where c.id="+carrera.id+";")
        return {response:{error:false,listaMaterias:listaMaterias}}
      }
      
      async lista():Promise<any>{
        return await this.imparteMateriasRepository.query("select im.id,nombre,primerApellido,segundoApellido,correoInstitucional,titulo,nombreMateria,t.id as idDocente,m.id as idMateria from impartematerias as im join trabajador as t on im.trabajadorImparteId=t.id join materias as m on m.id=im.materiasImparteId;")
      }

      async regstro(registro:any):Promise<any>{
        try{
          let siExiste:any[]= await this.imparteMateriasRepository.query("select * from impartematerias where trabajadorImparteId="+registro.idMateria+" and materiasImparteId="+registro.idTrabajador+";")
        if(siExiste.length>0){
          return {response:{error:true,message:"El registro ya existe"}}
        }else{                                                                                      
          await this.imparteMateriasRepository.query("insert into impartematerias values(null,"+registro.idMateria+","+registro.idTrabajador+");")
          return {response:{error:false,message:"Se registro correctamente"}}
        }
        }catch(err){
          return {response:{error:false,message:"Ocurrio un error interno",errorMessage:err}}
        }
      }

      async modificar(modificar:any):Promise<any>{
        try{
          let siExiste:any[]= await this.imparteMateriasRepository.query("select * from impartematerias where trabajadorImparteId="+modificar.idTrabajador+" and materiasImparteId="+modificar.idMateria+";")
        if(siExiste.length>0){
          return {response:{error:true,message:"El registro ya existe"}}
        }else{
          await this.imparteMateriasRepository.query("update impartematerias set trabajadorImparteId="+modificar.idTrabajador+",materiasImparteId="+modificar.idMateria+" where id="+modificar.id+";")
          return {response:{error:false,message:"Se registro correctamente"}}
        }
        }catch(err){
          return {response:{error:false,message:"Ocurrio un error interno",errorMessage:err}}
        }
      }

      async eliminar(imparteMaterias:any):Promise<any>{
        try {
          let sqlEliminarImparteMateria="delete from impartematerias where id = '"+imparteMaterias.id+"';"
          await this.imparteMateriasRepository.query(sqlEliminarImparteMateria)
          return {response:{error:false, message:"Imparte materias eliminado correctamente"}}
        } catch (err) {
          return {response:{error:true, message:"Error al eliminar imparte materias", errorMessage:err}}
        }
      }
}
