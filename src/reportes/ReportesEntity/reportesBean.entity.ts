import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';


@Entity('reportes')
export class ReportesEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  observacion:string

  @Column()
  imagenURL:string

  @Column()
  fecha:Date

  @Column()
  estusReporte:number

  @Column()
  usuarioID:number
  
}
