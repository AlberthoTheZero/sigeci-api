import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CarreraEntity } from './CarreraEntity/carreraBean.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CarreraService {
  constructor(
    @InjectRepository(CarreraEntity)
    private readonly carreraRepository: Repository<CarreraEntity>
  ) { }


  async lista(): Promise<any> {
    try {
      return await this.carreraRepository.query("select id,nombreCarrera,acronimoCarrera from carrera where status =1;")
    } catch (err) {
      return { response: { error: true, errorMessage: err } }
    }

  }


  async registroCarrera(carrera: any): Promise<any> {
    try {
      let sqlInsertarCarrera = "INSERT INTO carrera VALUES (null, '" + carrera.nombreCarrera + "', '" + carrera.acronimoCarrera + "',1);"
      await this.carreraRepository.query(sqlInsertarCarrera)
      return { respose: { error: false, message: "Registro de carrera correcto" } }
    } catch (err) {
      return { response: { error: true, message: "Error al insertar", errorMessage: err } }
    }
  }

  async modificarCarrera(carrera: any): Promise<any> {
    try {
      let sqlActualizarCarrera = "UPDATE carrera SET nombreCarrera = '" + carrera.nombreCarrera + "', acronimoCarrera = '" + carrera.acronimoCarrera + "', status = '" + carrera.status + "' WHERE id = '" + carrera.id + "';"
      await this.carreraRepository.query(sqlActualizarCarrera)
      return { response: { error: false, message: "Carrera modificada correctamente" } }
    } catch (err) {
      return { response: { error: true, message: "Error al modificar carrera", errorMessage: err } }
    }
  }

  async eliminarCarrera(carrera: any): Promise<any>{
    try{
      let sqlEliminarCarrera = "DELETE from carrera where id = '"+carrera.id+ "';"
      await this.carreraRepository.query(sqlEliminarCarrera)
      return { response: { error: false, message: "Carrera eliminada correctamente"} }
    }catch(err){
      return { response: { error: false, message: "No es posible eliminar la Carrera puesto que tiene alguna dependencia con otra sección" }}
    }
  }

}
