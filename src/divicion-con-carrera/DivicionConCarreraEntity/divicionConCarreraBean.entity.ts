import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne, Double, OneToOne, JoinColumn} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { type } from "os";
import { DivicionAcademicaEntity } from "../../divicion-academica/DivicionAcademicaEntity/divicionAcademicaBean.entity";
import { MateriaCarreraEntity } from "../../materia-carrera/MateriaCarreraEntity/materiaCarreraBean.entity";
import { CarreraEntity } from "../../carrera/CarreraEntity/carreraBean.entity";


@Entity('divicion_carrera')
export class DivicionConCarreraEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type=>DivicionAcademicaEntity,divicionConCarrera=>divicionConCarrera.asignaDivicion)
  divicionConCarrera:DivicionAcademicaEntity

  @ManyToOne(type=>CarreraEntity,carreraConDivicion=>carreraConDivicion.asignaCarrera)
  carreraConDivicion:DivicionConCarreraEntity

 
}
