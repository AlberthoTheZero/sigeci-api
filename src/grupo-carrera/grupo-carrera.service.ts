import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { GrupoCareraEntity } from './GrupoCarreraEntity/grupoCarreraBean.entity';

@Injectable()
export class GrupoCarreraService {


    constructor(
        @InjectRepository(GrupoCareraEntity)
        private readonly grupoCarreraRepository: Repository<GrupoCareraEntity>
      ) {}

      async listasParaRegistro():Promise<any>{
            try{
                let listaCarreras= await this.grupoCarreraRepository.query("select * from carrera;")
                let listaGrupos= await this.grupoCarreraRepository.query("select * from grupo;")
                return {response:{error:false,message:"Consulta exitosa",listaCarreras:listaCarreras,listaGrupos:listaGrupos}}
            }catch(err){
                return {response:{error:true,message:"Error al ejecutarse",errorMessage:err}}
            }
      }
      

      async registro(registro:any):Promise<any>{
        try{
           let buscarExite:any[]= await this.grupoCarreraRepository.query("select * from grupo_carrera where grupoConCarreraId="+registro.idGrupo+" and asignaGrupoCarreraId="+registro.idCarrera+";") 
            if(buscarExite.length>0){
                return {response:{error:true,message:"El registro ya existe"}}
            }else{
                await this.grupoCarreraRepository.query("insert into grupo_carrera values(null,"+registro.idGrupo+","+registro.idCarrera+");")
                return {response:{error:false,message:"El registro se realizo"}}
            }
        }catch(err){
            return {response:{error:true,message:"Error al ejecutarse",errorMessage:err}}
        }

      }

      async modificar(modificar:any):Promise<any>{
        try{
            let buscarExite:any[]= await this.grupoCarreraRepository.query("select * from grupo_carrera where grupoConCarreraId="+modificar.idGrupo+" and asignaGrupoCarreraId="+modificar.idCarrera+";") 
             if(buscarExite.length>0){
                 return {response:{error:true,message:"El registro ya existe"}}
             }else{
                 await this.grupoCarreraRepository.query("update grupo_carrera set grupoConCarreraId="+modificar.idGrupo+",asignaGrupoCarreraId="+modificar.idCarrera+" where id="+modificar.id+";")
                 return {response:{error:false,message:"El registro se realizo"}}
             }
         }catch(err){
             return {response:{error:true,message:"Error al ejecutarse",errorMessage:err}}
         }

      }

      async lista():Promise<any>{
        try{
            return await this.grupoCarreraRepository.query("select gc.id,gradoGrupo,letraGrupo,nombreCarrera,acronimoCarrera from grupo_carrera as gc join grupo as g on g.id=gc.grupoConCarreraId join carrera as c on gc.asignaGrupoCarreraId=c.id;")
        }catch(err){
            return {response:{error:true,message:"Error al ejecutarse",errorMessage:err}}
        }
      }

      async eliminar(grupoCarrera:any):Promise<any>{
        try {
          let sqlElminarGrupoCarrera="delete from grupo_carrera where id = '"+grupoCarrera.id+"';"
          await this.grupoCarreraRepository.query(sqlElminarGrupoCarrera)
          return {response:{error:false, message:"Grupo carrera eliminado correctamente"}}
        } catch (err) {
          return {response:{error:true, message:"Esta carrera tiente dependencias", errorMessage:err}}
        }
      }
}
