import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne, Double, OneToOne, JoinColumn} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { TrabajadorEntity } from "../../trabajador/trabajadorEntity/trabajadorBean.entity";
import { type } from "os";
import { AspiranteEntity } from "../../aspirantse/AspiranteEntity/aspiranteBean.entity";


@Entity('evaluacion')
export class EvaluacionEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  evaluacion:number

  @Column()
  comentrario:string

  @ManyToOne(type=>AspiranteEntity,aspiranteEvaluacion=>aspiranteEvaluacion.evaluacionAspirante)
  aspiranteEvaluacion:AspiranteEntity

  @ManyToOne(type=>TrabajadorEntity,trabajadorEvaluacion=>trabajadorEvaluacion.evaluacionTrabajador)
  trabajadorEvaluacion:TrabajadorEntity



}
