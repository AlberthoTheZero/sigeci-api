import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AspirantseController } from './aspirantse.controller';
import { AspirantseService } from './aspirantse.service';
import { AspiranteEntity } from './AspiranteEntity/aspiranteBean.entity';

@Module({
    imports: [TypeOrmModule.forFeature([AspiranteEntity])],
    controllers: [AspirantseController],
    providers: [AspirantseService]

})
export class AspiranteModule {}
