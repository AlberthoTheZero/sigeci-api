import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne } from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { type } from "os";
import { GrupoCarreraAspiranteEntity } from "../../grupo-carrera-aspirante/GrupoCarreraAspiranteEntity/grupoCarreraAspirante.entity";
import { EstadoAsistenciaEntity } from "../../estado-asistencia/estadoAsistenciaEntity/estadoAsistenciaBean.entity";
import { HorarioEntity } from "../../horario/HorarioEntity/horarioBean.entity";
import { AspiranteEntity } from "../../aspirantse/AspiranteEntity/aspiranteBean.entity";

@Entity('grupo_horario')
export class GrupoHorarioEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fecha: Date

  @Column()
  aspiranteId: number

  @ManyToOne(type => EstadoAsistenciaEntity, estadoAsistenciaGrupoHorario => estadoAsistenciaGrupoHorario.grupoHorarioAsistencia)
  estadoAsistenciaGrupoHorario: EstadoAsistenciaEntity

  @ManyToOne(type => HorarioEntity, horarioGrupoHorario => horarioGrupoHorario.grupoHorariohorario)
  horarioGrupoHorario: HorarioEntity




}
