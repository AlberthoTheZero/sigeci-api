import { Controller, Get, Post, Body } from '@nestjs/common';
import { GrupoService } from './grupo.service';

@Controller('grupo')
export class GrupoController {
    
    constructor(private grupoService: GrupoService) { }

    @Get('lista')
    async lista(): Promise<any> {
        return this.grupoService.lista();
    }

    @Post('registrar')
    async registrarGrupo(@Body('grupo') grupo: any): Promise<any> {
        if (!grupo) {
            return { error: true, message: "Objeto vacío" }
        }
        return await this.grupoService.registrarGrupo(grupo)
    }

    @Post('modificacion')
    async modificarCarrera(@Body('grupo') grupo: any): Promise<any> {
        if (!grupo) {
            return { error: true, message: "Objeto vacío" }
        }
        return await this.grupoService.modificarGrupo(grupo)
    }

    @Post('eliminacion')
    async eliminarGrupo(@Body('grupo') grupo: any): Promise<any> {
        if (!grupo) {
            return { error: true, message: "Objeto vacío" }
        }
        return await this.grupoService.eliminarGrupo(grupo)
    }

}
