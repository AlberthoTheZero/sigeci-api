import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CalificacionAspiranteEntity } from './CalificacionAspiranteEntity/calificacionAspiranteBean.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CalificacionAspiranteService {
    constructor(
        @InjectRepository(CalificacionAspiranteEntity)
        private readonly calificacionAspiranteRepository: Repository<CalificacionAspiranteEntity>
    ) { }


    async listaCalificaciones(): Promise<any> {
        try {
            let sqlConsultarCalificaciones = "select * from calificacionaspirante as ca join horario as h join aspirante as a where ca.idTrabajadorId=2 and h.id = 1 and ca.idMateriaId = 1 and ca.idAspiranteId = a.id;"
            return await this.calificacionAspiranteRepository.query(sqlConsultarCalificaciones)
        } catch (err) {
            return { response: { error: true, message: "Error al listar", errorMessage: err } }
        }
    }

    async registroCalificacionAspirante(registro: any): Promise<any> {
        try {
            let sqlRegistroCalificacionAspirante = "INSERT INTO calificacionaspirante VALUES (null,'" + registro.calificacion + "', '" + registro.idAspiranteId + "', '" + registro.idTrabajadorId + "');"
            await this.calificacionAspiranteRepository.query(sqlRegistroCalificacionAspirante)
            return { response: { error: false, message: "Registro exitoso" } }
        } catch (err) {
            return { response: { error: true, message: "Error al registrar", errorMessage: err } }
        }
    }

    async modificarCalificacionAspirante(modificar: any): Promise<any> {
        try {
            let sqlModificarCalificacionAspirante = "UPDATE calificacionaspirante SET calificacion = '" + modificar.calificacion + "' WHERE id = '" + modificar.id + "';"
            await this.calificacionAspiranteRepository.query(sqlModificarCalificacionAspirante)
            return { response: { error: false, message: "Modificación exitosa." } }
        } catch (err) {
            return { response: { error: true, message: "Error al modificar", errorMessage: err } }
        }
    }

    async eliminarCalificacionAspirante(eliminar: any): Promise<any> {
        try {
            let sqlEliminarCalificacionAspirante = "delete from calificacionaspirante where id  = '" + eliminar.id + "';"
            await this.calificacionAspiranteRepository.query(sqlEliminarCalificacionAspirante)
            return { response: { error: false, message: "Eliminación exitosa" } }
        } catch (err) {
            return { response: { error: true, message: "Error al eliminar", errorMessage: err } }
        }
    }

    async registrarCalificacionesAspirantes(calificaciones: any): Promise<any> {
        try {
            for (let aspirante of calificaciones) {
                let sqlRegistroCalificacionesAspirantes = "INSERT INTO calificacionaspirante VALUES (null,'" + aspirante.calificacion + "', '" + aspirante.idAspiranteId + "', '" + aspirante.idTrabajadorId + "', '" + aspirante.idMateriaId + "');"
                await this.calificacionAspiranteRepository.query(sqlRegistroCalificacionesAspirantes)
            }
            return { response: { error: false, message: "Registro exitoso" } }
        } catch (err) {
            return { response: { error: false, message: "Error al registrar calificaciones", errorMessage: err } }
        }
    }

    async listaCalificacionesProfesor(profesor: any): Promise<any> {
        try {
            let sqlConsultarCalificacionesProfesor = "select ca.id, a.id as 'id_aspirante',a.nombre,a.primerApellido,a.segundoApellido, ca.calificacion  from calificacionaspirante as ca join horario as h join aspirante as a where ca.idTrabajadorId= '" + profesor.id + "' and h.id = '" + profesor.idHorario + "' and ca.idAspiranteId = a.id;"
            return await this.calificacionAspiranteRepository.query(sqlConsultarCalificacionesProfesor)
        } catch (err) {
            return { response: { error: true, message: "Error al cargar la lista", errorMessage: err } }
        }
    }

    async listaCalificacionesAspirante(aspirante: any): Promise<any> {
        try {
            let sqlConsultarCalificacionesAspirante = "select ca.id, m.nombreMateria, ca.calificacion from calificacionaspirante as ca  join materias as m where ca.idAspiranteId= '"+aspirante.id+"';"
            return await this.calificacionAspiranteRepository.query(sqlConsultarCalificacionesAspirante)
        } catch (err) {
            return { response: { error: true, message: "Error al listar", errorMessage: err } }
        }

    }
}
