import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { MateriaCarreraEntity } from "../../materia-carrera/MateriaCarreraEntity/materiaCarreraBean.entity";
import { type } from "os";
import { ImparteMateriasEntity } from "../../imparte-materias/imparteMateriasEntity/imparteMateriasBean.entity";
import { CalificacionAspiranteEntity } from "../../calificacion-aspirante/CalificacionAspiranteEntity/calificacionAspiranteBean.entity";

@Entity('materias')
export class MateriasEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombreMateria:string

  @OneToMany(type=>ImparteMateriasEntity,imparteMaterias=>imparteMaterias.materiasImparte)
  imparteMaterias:ImparteMateriasEntity

  @OneToMany(type=>MateriaCarreraEntity,asignaMateria=>asignaMateria.materiasAsignaCarrera)
  asignaMateria:MateriaCarreraEntity

  @OneToMany(type=>CalificacionAspiranteEntity,calificacion=>calificacion.id_materia)
  calificacion :CalificacionAspiranteEntity
 
}
