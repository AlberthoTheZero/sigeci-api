import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { TrabajadorEntity } from "../../trabajador/trabajadorEntity/trabajadorBean.entity";


@Entity('tiempo')
export class TiempoEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombreTiempo:string

  @OneToMany(type=>TrabajadorEntity,trabajadorTiempo=>trabajadorTiempo.tiempoTrabajador)
  trabajadorTiempo:TrabajadorEntity

}
