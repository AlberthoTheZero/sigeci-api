import { Injectable, Body } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DivicionAcademicaEntity } from './DivicionAcademicaEntity/divicionAcademicaBean.entity';

@Injectable()
export class DivicionAcademicaService {
  constructor(
    @InjectRepository(DivicionAcademicaEntity)
    private readonly divicionRepository: Repository<DivicionAcademicaEntity>
  ) { }

  async lista(): Promise<any> {
    try {
      return await this.divicionRepository.query("select * from divicionacademica;");
    } catch (err) {
      return { response: { error: true, errorMessage: err } }
    }
  }

  async registroDivicion(divicion: any): Promise<any> {
    try {
      let existDivicion: any[] = await this.divicionRepository.query("select * from divicionacademica where nombreDivicion = '" + divicion.nombreDivicion + "';")
      if (existDivicion.length > 0) {
        return { response: { error: true, message: "Ya existe la división académica" } }
      } else {
        await this.divicionRepository.query("insert into divicionacademica values (null,'" + divicion.nombreDivicion + "');")
        return { response: { error: false, message: "Registro de division correcto" } }
      }
    } catch (err) {
      return { response: { error: true, message: "Error al registrar división", errorMessage: err } }
    }
  }

  async modificarDivicion(divicion: any): Promise<any> {
    try {
      let existDivicion: any[] = await this.divicionRepository.query("select * from divicionacademica where nombreDivicion = '" + divicion.nombreDivicion + "';")
      if (existDivicion.length>0) {
        return { response: { error: true, message: "Ya existe la división académica" } }
      } else {
        await this.divicionRepository.query("UPDATE divicionacademica SET nombreDivicion = '" + divicion.nombreDivicion + "' WHERE id = '" + divicion.id + "'; ")
        return { respose: { error: false, message: "Modificación de division correcto" } }
      }
    } catch (err) {
      return { response: { error: true, message: "Error al modificar división", errorMessage: err } }
    }
  }

  async eliminarDivisionAcademica(divicion:any):Promise<any>{
    try {
      let sqlElminarDivicionAcademica="delete from divicionacademica where id = '"+divicion.id+"';"
      await this.divicionRepository.query(sqlElminarDivicionAcademica)
      return {response:{error:false, message:"División académica eliminado correctamente"}}
    } catch (err) {
      return {response:{error:true, message:"No es posible eliminar la División puesto que tiene alguna dependencia con otra sección", errorMessage:err}}
    }
  }
}