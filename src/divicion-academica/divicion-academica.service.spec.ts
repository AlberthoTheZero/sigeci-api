import { Test, TestingModule } from '@nestjs/testing';
import { DivicionAcademicaService } from './divicion-academica.service';

describe('DivicionAcademicaService', () => {
  let service: DivicionAcademicaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DivicionAcademicaService],
    }).compile();

    service = module.get<DivicionAcademicaService>(DivicionAcademicaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
