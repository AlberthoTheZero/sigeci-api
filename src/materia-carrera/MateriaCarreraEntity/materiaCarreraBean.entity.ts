import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { type } from "os";
import { MateriasEntity } from "../../materias/MateriasEntity/materiaBean.entity";
import { CarreraEntity } from "../../carrera/CarreraEntity/carreraBean.entity";


@Entity('materiaCarrera')
export class MateriaCarreraEntity {

  @PrimaryGeneratedColumn()
  id: number;

 
  @ManyToOne(type=>MateriasEntity,materiasAsigaCarrera=>materiasAsigaCarrera.asignaMateria)
  materiasAsignaCarrera:MateriasEntity  

  @ManyToOne(type=>CarreraEntity,carreraAsignaMaterias=>carreraAsignaMaterias.asignaCarrera)
  carreraAsignaMaterias:CarreraEntity
}
