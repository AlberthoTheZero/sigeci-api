import { Module } from '@nestjs/common';
import { MateriaCarreraController } from './materia-carrera.controller';
import { MateriaCarreraService } from './materia-carrera.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MateriaCarreraEntity } from './MateriaCarreraEntity/materiaCarreraBean.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MateriaCarreraEntity])],
  controllers: [MateriaCarreraController],
  providers: [MateriaCarreraService]
})
export class MateriaCarreraModule {}
