import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany, ManyToOne} from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import * as crypto from 'crypto';
import { type } from "os";
import { RolEntity } from "../../rol/RolEntity/rolBean.entity";


@Entity('usuario')
export class UsuarioEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  usuarioNombre:string

  @Column()
  clave:string

  @Column("int",{default:1})
  estatus:number
  
  @ManyToOne(type=>RolEntity,rolUsuario=>rolUsuario.usuarioRol)
  rolUsuario:RolEntity
}
